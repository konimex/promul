<?= $this->extend('partials/default2') ?>

<?= $this->section('content') ?>

<?php $forminputs = session()->getFlashdata('forminputs'); ?>

<!-- Search Section Begin -->
<section class="search-section spad">
	<div class="container">
		<?= form_open(base_url('property/search')) ?>
		<div class="row">
			<div class="col-lg-7">
				<div class="section-title">
					<h4>Cari Hunian Sekitar Universitas Pertamina?</h4>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="change-btn">
					<div class="cb-item">
						<label for="tersedia" class="<?= $available ? 'active' : '' ?>">
							Tersedia
							<input type="radio" name="available" value="1" id="tersedia">
						</label>
					</div>
					<div class="cb-item">
						<label for="penuh" class="<?= $available ? '' : 'active' ?>">
							Penuh
							<input type="radio" name="available" value="0" id="penuh">
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="search-form-content">
			<div class="filter-form">
				<select name="tipe_properti" class="sm-width">
					<option disabled <?= !isset($forminputs['tipe_properti']) ? 'selected' : '' ?>>Pilih Property</option>
					<option value="Villa" <?= isset($forminputs['tipe_properti']) && $forminputs['tipe_properti'] == 'Villa' ? 'selected' : '' ?>>Vila</option>
					<option value="Kontrakan" <?= isset($forminputs['tipe_properti']) && $forminputs['tipe_properti'] == 'Kontrakan' ? 'selected' : '' ?>>Kontrakan</option>
					<option value="Kos" <?= isset($forminputs['tipe_properti']) && $forminputs['tipe_properti'] == 'Kos' ? 'selected' : '' ?>>Kos</option>
				</select>
				<select name="occupant_type" class="sm-width">
					<option disabled <?= !isset($forminputs['occupant_type']) ? 'selected' : '' ?>>Pilih Jenis Penghuni</option>
					<option value="Putra" <?= isset($forminputs['occupant_type']) && $forminputs['occupant_type'] == 'Putra' ? 'selected' : '' ?>>Laki-laki</option>
					<option value="Putri" <?= isset($forminputs['occupant_type']) && $forminputs['occupant_type'] == 'Putri' ? 'selected' : '' ?>>Perempuan</option>
					<option value="Bebas" <?= isset($forminputs['occupant_type']) && $forminputs['occupant_type'] == 'Bebas' ? 'selected' : '' ?>>Campur</option>
				</select>
				<div class="price-range-wrap sm-width">
					<div class="price-text">
						<label for="priceRange">Price:</label>
						<input type="text" id="priceRange" name="priceRange" readonly style="width: 200px;">
					</div>
					<div id="price-range" class="slider"></div>
				</div>
				<input name="keyword" class="xl-width" style="height: 46px; padding: .375rem .75rem; border: 1px solid #ced4da; transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out; margin-right: 20px; font-family: inherit; font-size: 14px; font-weight: normal;" placeholder="Cari lokasi" value="<?= isset($forminputs['keyword']) ? $forminputs['keyword'] : '' ?>" />
				<button type="submit" class="search-btn sm-width">Search</button>
			</div>
		</div>
		<div class="more-option">
			<div class="accordion" id="accordionExample">
				<div class="card">
					<div class="card-heading active">
						<a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true">
							Fasilitas Property
						</a>
					</div>
					<div id="collapseOne" class="collapse show" data-parent="#accordionExample">
						<div class="card-body">
							<div class="mo-list">
								<div class="ml-column">
									<label for="ac">Air conditioning
										<input name="ac" type="checkbox" id="ac" value="1" <?= isset($forminputs['ac']) ? 'checked' : '' ?>>
										<span class="checkbox"></span>
									</label>
									<label for="internet">Wifi
										<input name="internet" type="checkbox" id="internet" value="1" <?= isset($forminputs['internet']) ? 'checked' : '' ?>>
										<span class="checkbox"></span>
									</label>
								</div>
								<div class="ml-column">
									<label for="lemari">Lemari
										<input name="lemari" type="checkbox" id="lemari" value="1" <?= isset($forminputs['lemari']) ? 'checked' : '' ?>>
										<span class="checkbox"></span>
									</label>
									<label for="kamarmandi">Kamar Mandi
										<input name="kamarmandi" type="checkbox" id="kamarmandi" value="1" <?= isset($forminputs['kamarmandi']) ? 'checked' : '' ?>>
										<span class="checkbox"></span>
									</label>
								</div>
								<div class="ml-column">
									<label for="kasur">Kasur
										<input name="kasur" type="checkbox" id="kasur" value="1" <?= isset($forminputs['kasur']) ? 'checked' : '' ?>>
										<span class="checkbox"></span>
									</label>
									<label for="dapur_bersama">Dapur Bersama
										<input name="dapur_bersama" type="checkbox" id="dapur_bersama" value="1" <?= isset($forminputs['dapur_bersama']) ? 'checked' : '' ?>>
										<span class="checkbox"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?= form_close() ?>
	</div>
</section>
<!-- Search Section End -->

<!-- Property Section Begin -->
<section class="property-section spad">
	<div class="container">
		<?php if (!empty($searchResults)) : ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title">
						<h4>Search Results</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<?php $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY);
				$fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 0); ?>
				<?php foreach ($searchResults as $result) : ?>
					<?php
					$imgName = "";
					foreach ($propertiImg as $rowImg) {
						if ($result['id']==$rowImg['id_properti']) {
							$imgName = $rowImg['img_name'];
							break;
						}
					}
					?>
					<div class="col-lg-4 col-md-6">
						<div class="property-item">
							<div class="pi-pic set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$result['id'].'/'.$imgName); ?>">
								<?php if ($result['available']) : ?>
									<div class="label">Tersedia</div>
								<?php else : ?>
									<div class="label c-red">Penuh</div>
								<?php endif; ?>
							</div>
							<div class="pi-text">
								<div class="pt-price">
									<?= numfmt_format_currency($fmt, $result['price'], "IDR"); ?>
								</div>
								<h5><a href="<?= base_url('property/detail/'.$result['tipe_properti'].'/'.$result['id']) ?>"><?= $result['name'] ?></a></h5>
								<p><span class="icon_pin_alt"></span> <?= $result['address'] ?></p>
								<ul>
									<li><i class="fa fa-wifi"></i> <?= $result['internet'] ? 'Yes' : 'No' ?></li>
									<li><i class="fa fa-bathtub"></i> <?= $result['kamarmandi'] ? 'Yes' : 'No' ?></li>
									<li><i class="fa fa-bed"></i> <?= $result['kasur'] ? 'Yes' : 'No' ?></li>
									<li><i class="fa fa-spinner"></i> <?= $result['ac'] ? 'Yes' : 'No' ?></li>
								</ul>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<!-- <div class="col-lg-12">
					<div class="loadmore-btn">
						<a href="#">Load more</a>
					</div>
				</div> -->
			</div>
		<?php endif; ?>
	</div>
</section>
<!-- Property Section End -->

<?= $this->endSection() ?>
