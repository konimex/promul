<?= $this->extend('partials/default2') ?>

<?= $this->section('content') ?>

    <!-- All Property Section Begin -->
    <section class="property-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h4>All PROPERTY</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach($properti as $latestPrpt): ?>
                <div class="col-lg-4 col-md-6">
                    <div class="property-item">
                        <?php 
                        $imgProperti = "";
                        foreach($propertiImg as $prptImg) {
                            if ($latestPrpt['id'] == $prptImg['id_properti']) {
                                $imgProperti = $prptImg['img_name'];
                                break;
                            }
                        } ?>
                        <div class="pi-pic set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$latestPrpt['id'].'/'.$imgProperti); ?>">
                            <div class="<?php if($latestPrpt['tipe_properti'] == 'Villa') {echo 'label';} elseif($latestPrpt['tipe_properti'] == 'Kos') {echo 'label c-red';} elseif($latestPrpt['tipe_properti'] == 'Kontrakan') {echo 'label c-magenta';} ?>"><?php if($latestPrpt['tipe_properti'] == 'Villa') {echo 'Vila';} elseif($latestPrpt['tipe_properti'] == 'Kos') {echo 'Kos';} elseif($latestPrpt['tipe_properti'] == 'Kontrakan') {echo 'Kontrakan';} ?></div>
                        </div>
                        <div class="pi-text">
                            <?php 
                                $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY);
                                $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
                            ?>
                            <div class="pt-price"><?php echo numfmt_format_currency($fmt, $latestPrpt['price'], "IDR"); ?></div>
                            <h5><a href="<?= base_url('property/detail/'.$latestPrpt['tipe_properti'].'/'.$latestPrpt['id']) ?>"><?= $latestPrpt['name'] ?></a></h5>
                            <p><span class="icon_pin_alt"></span> <?= $latestPrpt['address'] ?></p>
                            <ul>
                                <li><i class="fa fa-wifi"></i> <?php if($latestPrpt['internet'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-bathtub"></i> <?php if($latestPrpt['kamarmandi'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-bed"></i> <?php if($latestPrpt['kasur'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-spinner"></i> <?php if($latestPrpt['ac'] == 1) echo "Yes"; else echo "No"; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?= $pager->links('properti', 'carikosdipertamina'); ?>
        </div>
    </section>
    <!-- All Property Section End -->

<?= $this->endSection() ?>