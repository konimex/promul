<?= $this->extend('partials/default') ?>

<?= $this->section('content') ?>

    <!-- Slider Section Begin -->
    <section class="hero-section">
        <div class="container">
            <div class="hs-slider owl-carousel">

                <?php 
                $VilaKasur = 0;
                $VilaKamarMandi = 0;
                $VilaInternet = 0;
                $VilaAC = 0;

                $KosKasur = 0;
                $KosKamarMandi = 0;
                $KosInternet = 0;
                $KosAC = 0;

                $KontrakanKasur = 0;
                $KontrakanKamarMandi = 0;
                $KontrakanInternet = 0;
                $KontrakanAC = 0;

                foreach($properti as $pr) {
                    if($pr['tipe_properti'] == "Villa") {
                        if($pr['kasur'] == 1) {
                            $VilaKasur++;
                        } if ($pr['kamarmandi'] == 1) {
                            $VilaKamarMandi++;
                        } if ($pr['internet'] == 1) {
                            $VilaInternet++;
                        } if ($pr['ac'] == 1) {
                            $VilaAC++;
                        }
                    } if($pr['tipe_properti'] == "Kos") {
                        if($pr['kasur'] == 1) {
                            $KosKasur++;
                        } if ($pr['kamarmandi'] == 1) {
                            $KosKamarMandi++;
                        } if ($pr['internet'] == 1) {
                            $KosInternet++;
                        } if ($pr['ac'] == 1) {
                            $KosAC++;
                        }
                    } if($pr['tipe_properti'] == "Kontrakan") {
                        if($pr['kasur'] == 1) {
                            $KontrakanKasur++;
                        } if ($pr['kamarmandi'] == 1) {
                            $KontrakanKamarMandi++;
                        } if ($pr['internet'] == 1) {
                            $KontrakanInternet++;
                        } if ($pr['ac'] == 1) {
                            $KontrakanAC++;
                        }
                    } 
                } 
                ?>

                <?php $number = 0; ?>
                <?php foreach($slider as $row) : ?>
                <?php $number++; ?>
                <div class="hs-item set-bg" data-setbg="<?= base_url('uploads/slider/'.$row['img_name']) ?>">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hc-inner-text">
                                <div class="hc-text">
                                    <?php if($number == 1) :?>
                                    <h4>Vila</h4>
                                    <p><span class="arrow_carrot-up"></span> Cocok banget untuk makrab</p>
                                    <div class="label">Kapasitas</div>
                                    <h5>Up to 100 orang<span>/properti</span></h5>
                                    <?php elseif($number == 2): ?>
                                    <h4>Kos</h4>
                                    <p><span class="arrow_carrot-up"></span> Anak rantau? Ngekos aja</p>
                                    <div class="label">Kapasitas</div>
                                    <h5>1 - 2 orang<span>/properti</span></h5>
                                    <?php else : ?>
                                     <h4>Kontrakan</h4>
                                    <p><span class="arrow_carrot-up"></span> Bisa diisi bersama teman satu prodi atau satu daerah</p>
                                    <div class="label">Kapasitas</div>
                                    <h5>Min 4 orang<span>/properti</span></h5>
                                    <?php endif; ?>   
                                </div>
                                <div class="hc-widget">
                                    <ul>
                                        <?php if($number == 1) :?>
                                        <li><i class="fa fa-wifi"></i> <?= $VilaInternet ?></li>
                                        <li><i class="fa fa-bathtub"></i> <?= $VilaKamarMandi ?></li>
                                        <li><i class="fa fa-bed"></i> <?= $VilaKasur ?></li>
                                        <li><i class="fa fa-spinner"></i> <?= $VilaAC ?></li>
                                        <?php elseif($number == 2): ?>
                                        <li><i class="fa fa-wifi"></i> <?= $KosInternet ?></li>
                                        <li><i class="fa fa-bathtub"></i> <?= $KosKamarMandi ?></li>
                                        <li><i class="fa fa-bed"></i> <?= $KosKasur ?></li>
                                        <li><i class="fa fa-spinner"></i> <?= $KosAC ?></li>
                                        <?php else : ?>
                                        <li><i class="fa fa-wifi"></i> <?= $KontrakanInternet ?></li>
                                        <li><i class="fa fa-bathtub"></i> <?= $KontrakanKamarMandi ?></li>
                                        <li><i class="fa fa-bed"></i> <?= $KontrakanKasur ?></li>
                                        <li><i class="fa fa-spinner"></i> <?= $KontrakanAC ?></li>
                                        <?php endif; ?>  
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!-- Slider Section End -->

    <!-- Search Section Begin -->
    <section class="search-section spad">
        <form method="post" action="<?= base_url('property/search') ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="section-title">
                        <h4>Cari Hunian Sekitar Universitas Pertamina?</h4>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="change-btn">
                        <div class="cb-item">
                            <label for="tersedia" class="active">
                                Tersedia
                                <input name="available" type="radio" id="tersedia" value="1">
                            </label>
                        </div>
                        <div class="cb-item">
                            <label for="penuh">
                                Penuh
                                <input name="available" type="radio" id="penuh" value="0">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search-form-content">  
                <div class="filter-form"> 
                    <select name="tipe_properti" class="sm-width">
                        <option selected disabled>Pilih Property</option>
                        <option value="Villa">Vila</option>
                        <option value="Kontrakan">Kontrakan</option>
                        <option value="Kos">Kos</option>
                    </select>
                    <select name="occupant_type" class="sm-width">
                        <option selected disabled>Pilih Jenis Penghuni</option>
                        <option value="Putra">Laki-laki</option>
                        <option value="Putri">Perempuan</option>
                        <option value="Bebas">Campur</option>
                    </select>
                    <div class="price-range-wrap sm-width">
                        <div class="price-text">
                            <label for="priceRange">Price: </label>
                            <input type="text" id="priceRange" name="priceRange" readonly style="width: 200px;">
                        </div>
                        <div id="price-range" class="slider"></div>
                    </div>
                    <input class="xl-width" style="height: 46px; padding: .375rem .75rem; border: 1px solid #ced4da; transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out; margin-right: 20px; font-family: inherit; font-size: 14px; font-weight: normal;" placeholder="Cari lokasi" name="keyword"/>
                    <button type="submit" class="search-btn sm-width">Search</button>
                </div>
            </div>
            <div class="more-option">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-heading active">
                            <a data-toggle="collapse" data-target="#collapseOne">
                                Fasilitas Property
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="mo-list">
                                    <div class="ml-column">
                                        <label for="ac">Air conditioning
                                            <input name="ac" type="checkbox" id="ac" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                        <label for="wifi">Wifi
                                            <input name="internet" type="checkbox" id="wifi" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                    </div>
                                    <div class="ml-column">
                                        <label for="almari">Almari
                                            <input name="lemari" type="checkbox" id="almari" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                        <label for="kamar_mandi">Kamar Mandi
                                            <input name="kamarmandi" type="checkbox" id="kamar_mandi" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                    </div>
                                    <div class="ml-column">
                                        <label for="kasur">Kasur
                                            <input name="kasur" type="checkbox" id="kasur" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                        <label for="dapur_bersama">Dapur Bersama
                                            <input name="dapur_bersama" type="checkbox" id="dapur_bersama" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </section>
    <!-- Search Section End -->

    <!-- Mengapa CariKos di Peramina Section Begin -->
    <section class="chooseus-section spad set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/chooseus/chooseus-bg.jpg'); ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="chooseus-text">
                        <div class="section-title">
                            <h4>Why choose us</h4>
                        </div>
                        <p>CariKos di Pertamina membantu kalian dalam memenuhi kebutuhan tempat tinggal di sekitar Universitas Pertamina dengan menyediakan informasi hunian di sekitar Universitas Pertamina</p>
                    </div>
                    <div class="chooseus-features">
                        <?php foreach($mengapa as $promosi): ?>
                        <div class="cf-item">
                            <div class="cf-pic">
                                <img src="<?php echo base_url('uploads/mengapa/'.$promosi['icon']); ?>" alt="">
                            </div>
                            <div class="cf-text">
                                <h5><?= $promosi['title'] ?></h5>
                                <p><?= $promosi['description'] ?></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Mengapa CariKos di Peramina Section End -->

    <!-- Testimonial Section Begin -->
    <section class="testimonial-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h4>What our client says?</h4>
                    </div>
                </div>
            </div>
            <div class="row testimonial-slider owl-carousel">
                <?php foreach($testimoni as $testm): ?>
                <div class="col-lg-6">
                    <div class="testimonial-item">
                        <div class="ti-text">
                            <p><?= $testm['testimony'] ?></p>
                        </div>
                        <div class="ti-author">
                            <div class="ta-pic">
                                <img src="<?php echo base_url('uploads/testimoni/'.$testm['foto']); ?>" alt="">
                            </div>
                            <div class="ta-text">
                                <h5><?= $testm['name'] ?></h5>
                                <div class="ta-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Latest Property Section Begin -->
    <section class="property-section latest-property-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="section-title">
                        <h4>Latest PROPERTY</h4>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="property-controls">
                        <ul>
                            <li data-filter="all">All</li>
                            <li data-filter=".vila">Vila</li>
                            <li data-filter=".kontrakan">Kontrakan</li>
                            <li data-filter=".kos">Kos</li>
                            <!-- <li data-filter=".hotel">Hotel</li>
                            <li data-filter=".restaurent">Restaurent</li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row property-filter">
                <?php $number = 0; foreach($properti as $latestPrpt): ?>
                <?php $number++; if($number > 6) break;?>
                <div class="col-lg-4 col-md-6 mix all <?php if($latestPrpt['tipe_properti'] == 'Villa') {echo 'vila';} elseif($latestPrpt['tipe_properti'] == 'Kos') {echo 'kos';} elseif($latestPrpt['tipe_properti'] == 'Kontrakan') {echo 'kontrakan';} ?>">
                    <div class="property-item">
                        <?php 
                        $imgProperti = "";
                        foreach($propertiImg as $prptImg) {
                            if ($latestPrpt['id'] == $prptImg['id_properti']) {
                                $imgProperti = $prptImg['img_name'];
                                break;
                            }
                        } ?>
                        <div class="pi-pic set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$latestPrpt['id'].'/'.$imgProperti); ?>">
                            <div class="<?php if($latestPrpt['tipe_properti'] == 'Villa') {echo 'label';} elseif($latestPrpt['tipe_properti'] == 'Kos') {echo 'label c-red';} elseif($latestPrpt['tipe_properti'] == 'Kontrakan') {echo 'label c-magenta';} ?>"><?php if($latestPrpt['tipe_properti'] == 'Villa') {echo 'Vila';} elseif($latestPrpt['tipe_properti'] == 'Kos') {echo 'Kos';} elseif($latestPrpt['tipe_properti'] == 'Kontrakan') {echo 'Kontrakan';} ?></div>
                        </div>
                        <div class="pi-text">
                            <div class="pt-price">
                                <?php 
                                $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY);
                                $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
                                echo numfmt_format_currency($fmt, $latestPrpt['price'], "IDR");
                                ?>
                            </div>

                            <?php $char = strlen($latestPrpt['name']);
                            if($char < 27) : ?>
                            <h5><a href="<?= base_url('property/detail/'.$latestPrpt['tipe_properti'].'/'.$latestPrpt['id']) ?>"><?= $latestPrpt['name'] ?></a></h5>
                            <?php else: ?>
                                <?php
                                $words = explode(" ",$latestPrpt['name']);
                                $word = implode(" ",array_splice($words,0,4));
                                ?>
                            <h5><a href="<?= base_url('property/detail/'.$latestPrpt['tipe_properti'].'/'.$latestPrpt['id']) ?>"><?= $word ?> ...</a></h5>    
                            <?php endif; ?>
                            
                            
                            <p><span class="icon_pin_alt"></span> <?= $latestPrpt['address'] ?></p>
                            <ul>
                                <li><i class="fa fa-wifi"></i> <?php if($latestPrpt['internet'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-bathtub"></i> <?php if($latestPrpt['kamarmandi'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-bed"></i> <?php if($latestPrpt['kasur'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-spinner"></i> <?php if($latestPrpt['ac'] == 1) echo "Yes"; else echo "No"; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!-- Latest Property Section End -->

    <!-- Top Property Section Begin -->
    <section class="feature-property-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 p-0">
                    <div class="feature-property-left">
                        <div class="section-title">
                            <h4>TOP PROPERTY</h4>
                        </div>
                        <ul>
                            <li>Vila</li>
                            <li>Kontrakan</li>
                            <li>Kos</li>
                        </ul>
                        <a href="<?= base_url('property') ?>">View all property</a>
                    </div>
                </div>
                <div class="col-lg-8 p-0">
                    <div class="fp-slider owl-carousel">
                        <?php foreach($properti as $topPrpt): ?>
                        <?php if($topPrpt['top_property'] == 1) : ?>
                            <?php 
                            $imgProperti = "";
                            foreach($propertiImg as $prptImg) {
                                if ($topPrpt['id'] == $prptImg['id_properti']) {
                                    $imgProperti = $prptImg['img_name'];
                                    break;
                                }
                            } ?>
                        <div class="fp-item set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$topPrpt['id'].'/'.$imgProperti); ?>">
                            <div class="fp-text">
                                <h5 class="title"><?= $topPrpt['name'] ?></h5>
                                <p><span class="icon_pin_alt"></span> <?= $topPrpt['address'] ?></p>
                                <div class="label"><?= $topPrpt['tipe_properti'] ?></div>
                                <h5><?= numfmt_format_currency($fmt, $latestPrpt['price'], "IDR"); ?></h5>
                                <ul>
                                    <li><i class="fa fa-wifi"></i> <?php if($latestPrpt['internet'] == 1) echo "Yes"; else echo "No"; ?></li>
                                    <li><i class="fa fa-bathtub"></i> <?php if($latestPrpt['kamarmandi'] == 1) echo "Yes"; else echo "No"; ?></li>
                                    <li><i class="fa fa-bed"></i> <?php if($latestPrpt['kasur'] == 1) echo "Yes"; else echo "No"; ?></li>
                                    <li><i class="fa fa-spinner"></i> <?php if($latestPrpt['ac'] == 1) echo "Yes"; else echo "No"; ?></li>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Top Property Section End -->

    <!-- Categories Section Begin -->
    <section class="categories-section">
        <div class="cs-item-list">
            <div class="cs-item set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/categories/cat-1.jpg'); ?>">
                <?php $jumlahProperti = $jumlahVilla + $jumlahKontrakan + $jumlahKos;  ?>
                <div class="cs-text">
                    <h5>All Property</h5>
                    <span><?= $jumlahProperti ?> property</span>
                </div>
            </div>
            <div class="cs-item set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/categories/cat-2.jpg'); ?>">
                <div class="cs-text">
                    <h5>Villa</h5>
                    <span><?= $jumlahVilla ?> property</span>
                </div>
            </div>
            <div class="cs-item set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/categories/cat-3.jpg'); ?>">
                <div class="cs-text">
                    <h5>Kontrakan</h5>
                    <span><?= $jumlahKontrakan ?> property</span>
                </div>
            </div>
            <div class="cs-item set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/categories/cat-5.jpg'); ?>">
                <div class="cs-text">
                    <h5>Kosan</h5>
                    <span><?= $jumlahKos ?> property</span>
                </div>
            </div>
        </div>
    </section>
    <!-- Categories Section End -->

    <!-- Team Section Begin -->
    <section id="OurTeam" class="team-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="section-title">
                        <h4>Our Team</h4>
                    </div>
                </div>
                <!-- <div class="col-lg-4 col-md-4">
                    <div class="team-btn">
                        <a href="#"><i class="fa fa-user"></i> ALL counselor</a>
                    </div>
                </div> -->
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="ts-item">
                        <div class="ts-text">
                            <img src="<?php echo base_url('frontoffice-assets/img/team/mayang.jpeg'); ?>" alt="">
                            <h5>Mayang Pratiwi</h5>
                            <span>CEO</span>
                            <p>Mahasiswa Universitas Pertamina Prodi Hubungan Internasional 2016</p>
                            <div class="ts-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ts-item">
                        <div class="ts-text">
                            <!-- <img src="<?php echo base_url('frontoffice-assets/img/team/team-2.jpg'); ?>" alt=""> -->
                            <h5 style="margin-top: 35%;">OUR TEAM</h5>
                            <span>Cari Kos di Pertamina</span>
                            <!-- <p>Ipsum dolor amet, consectetur adipiscing elit, eiusmod tempor incididunt lorem.</p>
                            <div class="ts-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ts-item">
                        <div class="ts-text">
                            <img src="<?php echo base_url('frontoffice-assets/img/team/mayang friends.jpeg'); ?>" alt="">
                            <h5>Dewa Ratih</h5>
                            <span>COO</span>
                            <p>Mahasiswa Universitas Pertamina Prodi Hubungan Internasional 2016</p>
                            <div class="ts-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Team Section End -->

<?= $this->endSection() ?>