<?= $this->extend('partials/default') ?>

<?= $this->section('content') ?>

<?php 
$contactWA = '';
$contactEmail = '';
$contactInstagram = '';
foreach($contact as $row) {
    $contactWA = $row['whatsapp'];
    $contactEmail = $row['email'];
    $contactInstagram = $row['instagram'];
} 
?>

	<?php $inputs = session()->getFlashdata('inputs'); ?>

	<!-- Property Details Section Begin -->
    <section class="property-details-section">
        <div class="property-pic-slider owl-carousel">
        	<?php 
        	$bnyakImg = 0; 
        	foreach ($propertiImg as $rowPrptImg) {
        		if($idProperti == $rowPrptImg['id_properti']) {
        			$bnyakImg++;
        		}
        	}
        	?>
        	<?php
        	// Untuk kelipatan 5
        	$grouping = $bnyakImg%5;
        	if($grouping == 0) :
        	?>

	        	<?php $number = 0; foreach($propertiImg as $rowPrptImg): ?>
				<?php if($idProperti == $rowPrptImg['id_properti']): ?>
            
					<?php
					// Untuk Img yang lebih dari 5 dan kelipatan 5
					if($number >= 5) $number = 0; ?>

                    	<?php $number++; ?>
                        <?php if($number==1): ?>
            <div class="ps-item">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-12 p-0">
                                    <div class="ps-item-inner large-item set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$idProperti.'/'.$rowPrptImg['img_name']); ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                    	<?php else: ?>
                                <div class="col-sm-6 p-0">
                                    <div class="ps-item-inner set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$idProperti.'/'.$rowPrptImg['img_name']); ?>"></div>
                                </div>
                        <?php endif; ?>
                        <?php if($number==5): ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>             	
                    	<?php endif; ?>
                        
	            <?php endif; ?>
	            <?php endforeach; ?>
            
            <!-- Untuk kelipatan 2 -->
            <?php else: ?>
            	<?php 
            		$grouping = $bnyakImg%2;
            		if($grouping == 0) :
            	?>

            	<?php $number = 0; foreach($propertiImg as $rowPrptImg): ?>
				<?php if($idProperti == $rowPrptImg['id_properti']): ?>

					<?php
					// Untuk Img yang lebih dari 2 dan kelipatan 2
					if($number >= 2) $number = 0; ?>

						<?php $number++; ?>
                        <?php if($number==1): ?>
            <div class="ps-item">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-12 p-0">
                                    <div class="ps-item-inner large-item set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$idProperti.'/'.$rowPrptImg['img_name']); ?>"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                    	<?php else: ?>
                                <div class="col-lg-12 p-0">
                                    <div class="ps-item-inner large-item set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$idProperti.'/'.$rowPrptImg['img_name']); ?>"></div>
                                </div>
                        <?php endif; ?>
                        <?php if($number==2): ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>             	
                    	<?php endif; ?>
                        
	            <?php endif; ?>
	            <?php endforeach; ?>

	            <!-- Untuk Kelipatan 1 -->
            	<?php else : ?>

            		<?php foreach($propertiImg as $rowPrptImg): ?>
					<?php if($idProperti == $rowPrptImg['id_properti']): ?>

			<div class="ps-item">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12 p-0">
                                    <div class="ps-item-inner large-item set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$idProperti.'/'.$rowPrptImg['img_name']); ?>"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

					<?php endif; ?>
	            	<?php endforeach; ?>

            	<?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="pd-text">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="pd-title">
                                    <div <?php if($inputs['tipe_properti'] == 'Villa') {echo 'class="label"';} elseif($inputs['tipe_properti'] == 'Kos') {echo 'class="label" style="background: #D41800;"';} elseif($inputs['tipe_properti'] == 'Kontrakan') {echo 'class= "label" style="background: #103230;"';} ?> ><?= $inputs['tipe_properti']; ?></div>
                                    <div class="pt-price">
                                    <?php 
                                    $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY);
                                    $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
                                    echo numfmt_format_currency($fmt, $inputs['price'], "IDR");
                                    ?>
                                    </div>
                                    <h3><?= $inputs['name'] ?></h3>
                                    <p><span class="icon_pin_alt"></span> <?= $inputs['address'] ?></p>
                                </div>
                            </div>
                            <!-- <div class="col-lg-6">
                                <div class="pd-social">
                                    <a href="#"><i class="fa fa-mail-forward"></i></a>
                                    <a href="#"><i class="fa fa-send"></i></a>
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                    <a href="#"><i class="fa fa-mail-forward"></i></a>
                                    <a href="#"><i class="fa fa-cloud-download"></i></a>
                                </div>
                            </div> -->
                        </div>
                        <div class="pd-board">
                            <div class="tab-board">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">Overview</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">Description</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab">Fasilitas</a>
                                    </li>
                                </ul><!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                        <div class="tab-details">
                                            <ul class="left-table">
                                                <li>
                                                    <span class="type-name">Tipe Properti</span>
                                                    <span class="type-value"><?= $inputs['tipe_properti'] ?></span>
                                                </li>
                                                <li>
                                                    <span class="type-name">Jenis Penghuni</span>
                                                    <span class="type-value"><?= $inputs['occupant_type'] ?></span>
                                                </li>
                                                <li>
                                                    <span class="type-name">Maks Penghuni</span>
                                                    <span class="type-value"><?= $inputs['maks_penghuni'] ?> orang</span>
                                                </li>
                                            </ul>
                                            <ul class="right-table">
                                                <li>
                                                    <span class="type-name">Ukuran Properti</span>
                                                    <span class="type-value"><?= $inputs['size'] ?></span>
                                                </li>
                                                <li>
                                                    <span class="type-name">Harga</span>
                                                    <span class="type-value"><?= numfmt_format_currency($fmt, $inputs['price'], "IDR"); ?></span>
                                                </li>
                                                <li>
                                                    <span class="type-name">Status</span>
                                                    <span class="type-value"><?php if($inputs['available']==1) echo "Tersedia"; else echo "Penuh"; ?></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tabs-2" role="tabpanel">
                                        <div class="tab-desc">
                                            <?= nl2br($inputs['other_info']); ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tabs-3" role="tabpanel">
                                        <div class="tab-details">
                                            <ul class="left-table">
                                                <li>
                                                    <span class="type-name">AC</span>
                                                    <span class="type-value"><?php if($inputs['ac'] == 1) echo "Tersedia"; else echo "Tidak Tersedia"; ?></span>
                                                </li>
                                                <li>
                                                    <span class="type-name">Kamar mandi Dalam</span>
                                                    <span class="type-value"><?php if($inputs['kamarmandi'] == 1) echo "Tersedia"; else echo "Tidak Tersedia"; ?></span>
                                                </li>
                                                <li>
                                                    <span class="type-name">Internet</span>
                                                    <span class="type-value"><?php if($inputs['internet'] == 1) echo "Tersedia"; else echo "Tidak Tersedia"; ?></span>
                                                </li>
                                            </ul>
                                            <ul class="right-table">
                                                <li>
                                                    <span class="type-name">Kasur</span>
                                                    <span class="type-value"><?php if($inputs['kasur'] == 1) echo "Tersedia"; else echo "Tidak Tersedia"; ?></span>
                                                </li>
                                                <li>
                                                    <span class="type-name">Lemari</span>
                                                    <span class="type-value"><?php if($inputs['lemari'] == 1) echo "Tersedia"; else echo "Tidak Tersedia"; ?></span>
                                                </li>
                                                <li>
                                                    <span class="type-name">Dapur Bersama</span>
                                                    <span class="type-value"><?php if($inputs['dapur_bersama'] == 1) echo "Tersedia"; else echo "Tidak Tersedia"; ?></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        	<a target="_blank" href="https://web.whatsapp.com/send?phone=<?= $contactWA ?> CariKos di Pertamina&amp;text=Halo, Saya mau booking hunian ini: <?= 'https://www.carikosdipertamina.com/property/detail/'.$inputs['tipe_properti'].'/'.$inputs['id'] ?>">
                        		<div style="background-color: #00C89E; font-size: 14px; color: #ffffff; font-weight: 600; text-transform: uppercase; width: 100%; margin-top: 15px; letter-spacing: 0.5px; padding: 14px 30px; text-align: center;" class="col-lg-12">
	                            	Booking Now
	                            </div>
                        	</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="property-sidebar">
                        <div class="single-sidebar">
                            <div class="section-title sidebar-title">
                                <h5>Top Property</h5>
                            </div>
                            <div class="top-agent">
                            	<?php $number = 0; foreach($properti as $topPrpt): ?>
		                        <?php if($topPrpt['top_property'] == 1) : ?>
		                            <?php 
		                            $number++;
		                            $imgProperti = "";
		                            foreach($propertiImg as $prptImg) {
		                                if ($topPrpt['id'] == $prptImg['id_properti']) {
		                                    $imgProperti = $prptImg['img_name'];
		                                    break;
		                                }
		                            } ?>
                                <div class="ta-item">
                                    <div class="ta-pic set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$topPrpt['id'].'/'.$imgProperti); ?>"></div>
                                    <div class="ta-text">
                                    	<?php $char = strlen($topPrpt['name']);
                                    	if($char < 33) : ?>
                                        <h6><a href="<?= base_url('property/detail/'.$topPrpt['tipe_properti'].'/'.$topPrpt['id']) ?>"><?= $topPrpt['name'] ?></a></h6>
                                        <?php else: ?>
		                                    <?php
		                                    $words = explode(" ",$topPrpt['name']);
		                                    $word = implode(" ",array_splice($words,0,3));
		                                    ?>
		                                <h6><a href="<?= base_url('property/detail/'.$topPrpt['tipe_properti'].'/'.$topPrpt['id']) ?>"><?= $word ?> ...</a></h6>    
	                                    <?php endif; ?>
                                        <span><?= numfmt_format_currency($fmt, $topPrpt['price'], "IDR"); ?></span>
                                        <div class="ta-num"><?= $topPrpt['tipe_properti'] ?></div>
                                    </div>
                                </div>
		                        <?php endif; ?>
		                        <?php 
		                        if($number==3) break;
		                        ?>
		                        <?php endforeach; ?>
                                <!-- <div class="ta-item">
                                    <div class="ta-pic set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/property/details/sidebar/ta-2'); ?>.jpg"></div>
                                    <div class="ta-text">
                                        <h6><a href="#">Ashton Kutcher</a></h6>
                                        <span>Team Leader</span>
                                        <div class="ta-num">123-455-688</div>
                                    </div>
                                </div>
                                <div class="ta-item">
                                    <div class="ta-pic set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/property/details/sidebar/ta-3'); ?>.jpg"></div>
                                    <div class="ta-text">
                                        <h6><a href="#">Ashton Kutcher</a></h6>
                                        <span>Team Leader</span>
                                        <div class="ta-num">123-455-688</div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Property Details Section End -->
<?= $this->endSection() ?>