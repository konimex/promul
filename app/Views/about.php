<?= $this->extend('partials/default3') ?>

<?= $this->section('content') ?>

    <!-- About Section Begin -->
    <section class="about-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-text">
                        <div class="at-title">
                            <h3>CariKos di Pertamina</h3>
                            <p>CariKos di Pertamina membantu kalian dalam memenuhi kebutuhan tempat tinggal di sekitar Universitas Pertamina dengan menyediakan informasi hunian di sekitar Universitas Pertamina.</p>
                        </div>
                        <div class="at-feature">
                            <?php foreach($mengapa as $promosi): ?>
                            <div class="af-item">
                                <div class="af-icon">
                                    <img src="<?php echo base_url('uploads/mengapa/'.$promosi['icon']); ?>" alt="">
                                </div>
                                <div class="af-text">
                                    <h6><?= $promosi['title'] ?></h6>
                                    <p><?= $promosi['description'] ?></p>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-pic set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/logo-ori.jpeg'); ?>">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    <!-- Team Section Begin -->
    <section id="OurTeam" class="team-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <div class="section-title">
                        <h4>Our Team</h4>
                    </div>
                </div>
                <!-- <div class="col-lg-4 col-md-4">
                    <div class="team-btn">
                        <a href="#"><i class="fa fa-user"></i> ALL counselor</a>
                    </div>
                </div> -->
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="ts-item">
                        <div class="ts-text">
                            <img src="<?php echo base_url('frontoffice-assets/img/team/mayang.jpeg'); ?>" alt="">
                            <h5>Mayang Pratiwi</h5>
                            <span>CEO</span>
                            <p>Mahasiswa Universitas Pertamina Prodi Hubungan Internasional 2016</p>
                            <div class="ts-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ts-item">
                        <div class="ts-text">
                            <!-- <img src="<?php echo base_url('frontoffice-assets/img/team/team-2.jpg'); ?>" alt=""> -->
                            <h5 style="margin-top: 35%;">OUR TEAM</h5>
                            <span>Cari Kos di Pertamina</span>
                            <!-- <p>Ipsum dolor amet, consectetur adipiscing elit, eiusmod tempor incididunt lorem.</p>
                            <div class="ts-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="ts-item">
                        <div class="ts-text">
                            <img src="<?php echo base_url('frontoffice-assets/img/team/mayang friends.jpeg'); ?>" alt="">
                            <h5>Dewa Ratih</h5>
                            <span>COO</span>
                            <p>Mahasiswa Universitas Pertamina Prodi Hubungan Internasional 2016</p>
                            <div class="ts-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-envelope-o"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Team Section End -->

    <!-- Testimonial Section Begin -->
    <section class="testimonial-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h4>What our client says?</h4>
                    </div>
                </div>
            </div>
            <div class="row testimonial-slider owl-carousel">
                <?php foreach($testimoni as $testm): ?>
                <div class="col-lg-6">
                    <div class="testimonial-item">
                        <div class="ti-text">
                            <p><?= $testm['testimony'] ?></p>
                        </div>
                        <div class="ti-author">
                            <div class="ta-pic">
                                <img src="<?php echo base_url('uploads/testimoni/'.$testm['foto']); ?>" alt="">
                            </div>
                            <div class="ta-text">
                                <h5><?= $testm['name'] ?></h5>
                                <div class="ta-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!-- Testimonial Section End -->

<?= $this->endSection() ?>