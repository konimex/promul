<?= $this->extend('backoffice/partials/default') ?>

<?= $this->section('content') ?>
<div class="col-md-6 col-sm-6">
	<div class="x_panel">
		<div class="x_title">
			<h2>Informasi - Mengapa CariKos di Pertamina</small></h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Icon</th>
						<th>Title</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $number = 0 ?>
				<?php foreach ($mengapa as $row) : ?>
					<?php $number += 1 ?>
					<tr>
						<th scope="row"><?= $number ?></th>
						<td><a href="<?= base_url('uploads/mengapa/'.$row['icon']) ?>">Klik disini untuk melihat gambar</a></td>
						<td><?= $row['title'] ?></td>
						<td><a href="<?php echo base_url('/backoffice/mengapa/edit/' . $row['id']); ?>"><i class="fa fa-pencil"></i></a></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

		</div>
	</div>
</div>
<?= $this->endSection() ?>
