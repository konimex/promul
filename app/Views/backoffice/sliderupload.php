<?= $this->extend('backoffice/partials/default') ?>

<?= $this->section('content') ?>
<div class="col-md-12 col-sm-12 col-xs-12">
	<?php
	$errors = session()->getFlashdata('errors');
	if (!empty($errors)) : ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= $error ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endif; ?>
	<div class="x_panel">
		<div class="x_title">
			<h2><?= $operation == 'new' ? 'New' : 'Edit' ?> Slider Image</h2>
			<div class="clearfix"></div>
		</div>
		<div style="margin: auto; width: 70%;">
			<div class="x_content">
				<?php if ($operation == 'new') : ?>
					<?= form_open_multipart(base_url('backoffice/slider/create')); ?>
				<?php elseif ($operation == 'edit') : ?>
					<?= form_open_multipart(base_url('backoffice/slider/update/' . $id)); ?>
				<?php endif; ?>
				<div class="form-group">
					<label for="gambar">File </label>
					<input type="file" name="gambar">
				</div>
				<br>
				<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
						<a href="<?= base_url('backoffice/slider') ?>" class="btn btn-primary">Cancel</a>
						<button type="submit" class="btn btn-success">Save</button>
					</div>
				</div>
				<?= form_close() ?>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>
