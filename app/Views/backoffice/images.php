<?= $this->extend('backoffice/partials/default') ?>

<?= $this->section('content') ?>
<div class="row">
	<div class="col-md-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?= $operation == 'edit' ? 'Edit' : 'Lihat' ?> Foto Properti <small> <?= $name ?> </small></h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="row">
					<?php $success = session()->getFlashdata('success');
					if (!empty($success)) : ?>
						<div class="alert alert-success alert-dismissible " role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<?= $success ?>
						</div>
					<?php endif; ?>
					<?php foreach ($images as $img) : ?>
						<div class="col-md-55">
							<div class="thumbnail" style="border:none">
								<div class="image view view-first">
									<img style="width: 100%; display: block;" src="<?= base_url('uploads/properti/' . $id . '/' . $img['img_name']) ?>" alt="image">
									<div class="mask">
										<p>​</p>
										<div class="tools tools-bottom">
											<?php if ($operation == 'view') : ?>
												<a href="<?= base_url('/backoffice/uploads/imgview/' . $img['id'])?>"><i class="fa fa-link"></i></a>
											<?php elseif ($operation == 'edit') : ?>
												<a href="<?= base_url('/backoffice/uploads/imgpreview/' . $img['id'])?>"><i class="fa fa-link"></i></a>
												<a href="<?= base_url('backoffice/uploads/imgnew/' . $img['id']) ?>"><i class="fa fa-pencil"></i></a>
												<a href="#" data-toggle="modal" data-target="#modal-danger<?= $img['id'] ?>"><i class="fa fa-times"></i></a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<div class="modal fade bs-example-modal-lg" id="modal-danger<?= $img['id'] ?>" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="myModalLabel">Peringatan</h4>
										</div>
										<div class="modal-body">
											<h4>Anda akan menghapus <strong><?= $img['img_name'] ?></strong></h4>
											<p>Apakah Anda yakin akan menghapus gambar tersebut?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
											<a class="btn btn-danger" href="<?= base_url('/backoffice/uploads/delete/' . $img['id']) ?>">Hapus</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="col-md-12">
					<div class="ln_solid"></div>
					<?php if ($operation == 'view') : ?>
					<a href="<?= base_url('/backoffice/properti/view/' . $id) ?>" class="btn btn-primary">Kembali</a>
					<?php elseif ($operation == 'edit') : ?>
					<a href="<?= base_url('/backoffice/properti/edit/' . $id) ?>" class="btn btn-primary">Kembali</a>
					<a href="<?= base_url('/backoffice/uploads/new/' . $id) ?>" class="btn btn-success">Upload Gambar Baru</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?= $this->endSection() ?>
