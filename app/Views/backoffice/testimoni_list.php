<?= $this->extend('backoffice/partials/default') ?>
<?= $this->section('content') ?>
<div class="x_panel">
	<?php
	$inputx = session()->getFlashdata('inputx');
	$errors = session()->getFlashdata('errors');
	$success = session()->getFlashdata('success');
	if (!empty($errors)) : ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= esc($error) ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endif;
	if (!empty($success)) : ?>
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<?= $success ?>
		</div>
	<?php endif; ?>
	<div class="x_title">
		<h2>List Testimoni</h2>
		<div class="clearfix"></div>
	</div>
	<div class="x_content">
		<table id="datatable" class="table table-striped table-bordered">
			<thead>
				<tr role="row">
					<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 5%;" aria-sort="ascending" aria-label="No: activate to sort column descending">No</th>
					<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 412px; Text-align:center" aria-label="Tipe Properti: activate to sort column ascending">Foto</th>
					<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 412px; Text-align:center" aria-label="Tipe Properti: activate to sort column ascending">Nama</th>
					<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 412px;Text-align:center" aria-label="Nama: activate to sort column ascending">Testimoni</th>
					<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 237px;Text-align:center" aria-label="Action: activate to sort column ascending">Action</th>
				</tr>
			</thead>
			<tbody><?php $number = 0 ?>
				<?php foreach ($testimoni as $row) : ?>
					<tr>
						<?php $number += 1 ?>
						<td><?= $number ?></td>
						<td style="text-align:center"><a href="<?= base_url('/uploads/testimoni/'. $row['foto']) ?>">Klik gambar untuk melihat foto</a></td>
						<td style="text-align:center"><?= $row['name'] ?></td>
						<td style="text-align:center"><?= $row['testimony'] ?></td>
						<td style="text-align:center"><a href="<?php echo base_url('/backoffice/testimoni/edit/' . $row['id']); ?>"><i class="fa fa-pencil"></i></a><a href="#" data-toggle="modal" data-target="#modal-danger<?= $row['id'] ?>" style="text-align:center"><i class="fa fa-times" style="margin-left:3px ;"></i></a></td>
					</tr>
					<div class="modal fade bs-example-modal-lg" id="modal-danger<?= $row['id'] ?>" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="myModalLabel">Peringatan</h4>
								</div>
								<div class="modal-body">
									<h4>Anda akan menghapus testimoni yang dibuat oleh <strong><?= $row['name'] ?></strong></h4>
									<p>Apakah Anda yakin akan menghapus testimoni tersebut?</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
									<a class="btn btn-danger" href="<?= base_url('/backoffice/testimoni/delete/' . $row['id']) ?>">Hapus</a>
								</div>
							</div>
						</div>
					</div>
			</tbody>
		<?php endforeach; ?>
		</table>
	</div>
</div>
<?= $this->endSection() ?>
