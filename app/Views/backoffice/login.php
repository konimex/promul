<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Back Office | CariKosPertamina</title>

	<!-- Bootstrap -->
	<link href="<?= base_url('backoffice-assets/includes/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?= base_url('backoffice-assets/includes/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?= base_url('backoffice-assets/includes/nprogress/nprogress.css') ?>" rel="stylesheet">
	<!-- Animate.css') ?> -->
	<link href="<?= base_url('backoffice-assets/includes/animate.min.css') ?>" rel="stylesheet">

	<!-- Custom Theme Style -->
	<link href="<?= base_url('backoffice-assets/includes/css/custom.min.css') ?>" rel="stylesheet">
</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>
		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form method="post" action="<?= base_url('backoffice/login/auth') ?>">
						<h1>BackOffice</h1>
						<?php
						$errors = session()->getFlashdata('errors');
						if(!empty($errors)) :?>
						<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
							</button>
							<?= $errors ?>
						</div>
						<?php endif; ?>
						<div>
							<input type="text" name="username" class="form-control" placeholder="Username" required="" />
						</div>
						<div>
							<input type="password" name="password" class="form-control" placeholder="Password" required="" />
						</div>
						<div>
							<button id="send" type="submit" class="btn btn-default submit">Login</button>
						</div>

						<div class="clearfix"></div>

						<div class="separator">
							<div class="clearfix"></div>
							<br />

							<div>
								<h1><i class="fa fa-paw"></i> Cari Kos Pertamina</h1>
								<p>©2020 CariKosPertamina</p>
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	</div>
</body>

</html>
