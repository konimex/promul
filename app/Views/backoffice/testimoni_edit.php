<?= $this->extend('backoffice/partials/default') ?>
<?= $this->section('content') ?>
<div class="x_panel">
	<?php
	$inputs = session()->getFlashdata('inputs');
	$errors = session()->getFlashdata('errors');
	$success = session()->getFlashdata('success');
	if (!empty($errors)) : ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= esc($error) ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endif;
	if (!empty($success)) : ?>
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<?= $success ?>
		</div>
	<?php endif; ?>
	<div class="x_title">
		<h2><?= $operation == 'new' ? 'Tambah' : 'Edit' ?> Testimoni</h2>
		<div class="clearfix"></div>
	</div>
	<div class="x_content">
		<?php $attributes = ['class' => 'form-horizontal form-label-left', 'id' => 'divTestimoni'];
			if ($operation == 'new') : ?>
			<?= form_open_multipart(base_url('backoffice/testimoni/create'), $attributes); ?>
		<?php elseif ($operation == 'edit') : ?>
			<?= form_open_multipart(base_url('backoffice/testimoni/update/' . $id), $attributes); ?>
		<?php endif; ?>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input class="form-control col-md-7 col-xs-12" name="name" id="fullname" value="<?= isset($inputs['name']) ? $inputs['name'] : '' ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="testimoni">Testimoni <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control col-md-7 col-xs-12" name="testimoni" id="description"><?= isset($inputs['testimony']) ? $inputs['testimony'] : '' ?></textarea>
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto">Foto <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input id="foto" name="foto" type="file" <?= $operation == 'new' ? 'required' : '' ?>>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<a href="<?= base_url('backoffice/testimoni') ?>" class="btn btn-primary">Cancel</a>
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</div>
		<?= form_close() ?>
	</div>
</div>
<?= $this->endSection() ?>
