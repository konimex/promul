<?= $this->extend('backoffice/partials/default') ?>

<?= $this->section('content') ?>
<div class="col-md-6 col-sm-6">
	<div class="x_panel">
		<div class="x_title">
			<h2>Slider</small></h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Gambar</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				<?php $number = 0; $title = ["Vila", "Kos", "Kontrakan"] ?>
				<?php foreach ($slider as $row) : ?>
					<?php $number += 1 ?>
					<tr>
						<th scope="row"><?= $number ?></th>
						<th><?php echo $title[$number-1] ?></th>
						<td><a href="<?= base_url('uploads/slider/'.$row['img_name']) ?>">Klik disini untuk melihat gambar</a></td>
						<td><a href="<?php echo base_url('/backoffice/slider/edit/' . $row['id']); ?>"><i class="fa fa-pencil"></i></a></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

		</div>
	</div>
</div>
<?= $this->endSection() ?>
