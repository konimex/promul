<?= $this->extend('backoffice/partials/default') ?>
<?= $this->section('content') ?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?php
			$inputs = session()->getFlashdata('inputs');
			$errors = session()->getFlashdata('errors');
			if(!empty($errors)) :?>
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<ul>
					<?php foreach ($errors as $error) : ?>
					<li><?= $error ?></li>
					<?php endforeach ?>
				</ul>
			</div>
			<?php endif; ?>
		<div class="x_panel">
			<div class="x_title">
				<h2><?php
				if($operation == 'new') echo 'Tambah';
				elseif($operation == 'edit') echo 'Edit';
				elseif($operation == 'view') echo 'View';?> Properti</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<?php if ($operation == 'new') echo form_open_multipart(base_url('backoffice/properti/create'), ['class' => 'form-horizontal form-label-left']);
				else echo form_open_multipart(base_url('backoffice/properti/update/'.$id), ['class' => 'form-horizontal form-label-left']);
				?>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Properti <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" type="text" value="<?= isset($inputs['name']) ? $inputs['name'] : '' ?>" <?= $operation == 'view' ? 'readonly' : '';?>>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="gambar">Foto Properti <span class="required">*</span></label>
						<?php if ($operation == 'new') :?>
						<input class="col-md-6 col-sm-6 col-xs-12" type="file" name="gambar[]" multiple required>

						<?php elseif ($operation == 'edit'): ?>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a class="btn btn-primary" href="<?= base_url('/backoffice/uploads/edit/'.$id) ?>">Edit Foto</a>
						</div>
						<?php else: ?>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a class="btn btn-primary" href="<?= base_url('/backoffice/uploads/view/'.$id) ?>">Lihat Foto</a>
						</div>
						<?php endif; ?>
					</div>
					<div class="item form-group">
						<label for="status" class="control-label col-md-3">Tipe Properti <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<?php if ($operation == 'new' || $operation == 'edit'): ?>
							<div class="radio">
								<label><input type="radio" name="tipe_properti" value="Kos" <?= isset($inputs['tipe_properti']) && $inputs['tipe_properti'] == 'Kos' ? 'checked' : '' ?>>Kos</label>
							</div>
							<div class="radio">
								<label><input type="radio" name="tipe_properti" value="Kontrakan" <?= isset($inputs['tipe_properti']) && $inputs['tipe_properti'] == 'Kontrakan' ? 'checked' : '' ?>>Kontrakan</label>
							</div>
							<div class="radio">
								<label><input type="radio" name="tipe_properti" value="Villa" <?= isset($inputs['tipe_properti']) && $inputs['tipe_properti'] == 'Villa' ? 'checked' : '' ?>>Villa</label>
							</div>
							<?php elseif ($operation == 'view'): ?>
								<input id="typeproperti" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="tipe_properti" type="text" value="<?= $inputs['tipe_properti'] ?>" readonly>
							<?php endif; ?>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="occupant_type">Jenis Penghuni <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="occupant_type" class="form-control" <?= $operation == 'view' ? 'disabled' : '' ?>>
								<option value="Bebas" <?= isset($inputs['occupant_type']) && $inputs['occupant_type'] == 'Bebas' ? 'selected' : '' ?>>Bebas</option>
								<option value="Putra" <?= isset($inputs['occupant_type']) && $inputs['occupant_type'] == 'Putra' ? 'selected' : '' ?>>Putra</option>
								<option value="Putri" <?= isset($inputs['occupant_type']) && $inputs['occupant_type'] == 'Putri' ? 'selected' : '' ?>>Putri</option>
							</select>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fasilitas">Fasilitas
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<?php if(!($operation == 'view')) :?>
								<div class="checkbox">
									<label><input name="ac" type="checkbox" value="1" <?= isset($inputs['ac']) && $inputs['ac'] ? 'checked' : '' ?>>AC</label>
								</div>
								<div class="checkbox">
									<label><input name="kamarmandi" type="checkbox" value="1" <?= isset($inputs['kamarmandi']) && $inputs['kamarmandi'] ? 'checked' : '' ?>>Kamar Mandi Dalam</label>
								</div>
								<div class="checkbox">
									<label><input name="internet" type="checkbox" value="1" <?= isset($inputs['internet']) && $inputs['internet'] ? 'checked' : '' ?>>Internet</label>
								</div>
								<div class="checkbox">
									<label><input name="kasur" type="checkbox" value="1" <?= isset($inputs['kasur']) && $inputs['kasur'] ? 'checked' : '' ?>>Kasur</label>
								</div>
								<div class="checkbox">
									<label><input name="lemari" type="checkbox" value="1" <?= isset($inputs['lemari']) && $inputs['lemari'] ? 'checked' : '' ?>>Lemari</label>
								</div>

								<div class="checkbox">
									<label><input name="dapur_bersama" type="checkbox" value="1" <?= isset($inputs['dapur_bersama']) && $inputs['dapur_bersama'] ? 'checked' : '' ?>>Dapur Bersama</label>
								</div>
							<?php else: ?>
								<?= $inputs['ac'] ? '<div class="checkbox"> - AC </div>' : '' ?>
								<?= $inputs['kamarmandi'] ? '<div class="checkbox">- Kamar Mandi Dalam</div>' : '' ?>
								<?= $inputs['internet'] ? '<div class="checkbox"> - Internet</div>' : '' ?>
								<?= $inputs['kasur'] ? '<div class="checkbox"> - Kasur</div>' : '' ?>
								<?= $inputs['lemari'] ? '<div class="checkbox"> - Lemari</div>' : '' ?>
								<?= $inputs['dapur_bersama'] ? '<div class="checkbox"> - Dapur Bersama</div>' : '' ?>
								<?= !$inputs['ac'] && !$inputs['kamarmandi'] && !$inputs['internet'] && !$inputs['kasur'] && !$inputs['lemari'] && !$inputs['dapur_bersama'] ? '<div class="checkbox">- Tidak ada</div>' : '' ?>
								<?php endif; ?>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="maks_penghuni">Maksimal Penghuni <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="number" name="maks_penghuni" class="form-control col-md-7 col-xs-12" value="<?= isset($inputs['maks_penghuni']) ? $inputs['maks_penghuni'] : '' ?>" <?= $operation == 'view' ? 'readonly' : '';?>>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="size">Ukuran Kos <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="website" name="size" placeholder="2 x 4 Meter" class="form-control col-md-7 col-xs-12" value="<?= isset($inputs['size']) ? $inputs['size'] : '' ?>" <?= $operation == 'view' ? 'readonly' : '';?>>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Harga <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input id="harga" type="text" name="price" class="optional form-control col-md-7 col-xs-12"
							value="<?php
							if($operation == 'view') {
								$fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY);
								$fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
								echo numfmt_format_currency($fmt, $inputs['price'], "IDR");
								} else { echo isset($inputs['price']) ? $inputs['price'] : ''; } ?>" <?= $operation == 'view' ? 'readonly' : '' ?>>
						</div>
					</div>
					<div class="item form-group">
						<label for="status" class="control-label col-md-3">Status <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<?php if ($operation == 'new' || $operation == 'edit'): ?>
							<div class="radio">
								<label><input type="radio" name="available" value="0" <?= isset($inputs['available']) && $inputs['available'] == '0' ? 'checked' : '' ?>>Penuh</label>
							</div>
							<div class="radio">
								<label><input type="radio" name="available" value="1" <?= isset($inputs['available']) && $inputs['available'] == '1' ? 'checked' : '' ?>>Tersedia</label>
							</div>
							<?php elseif ($operation == 'view'): ?>
								<input id="status" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="available" type="text" value="<?= $inputs['available'] ? 'Tersedia' : 'Penuh' ?>" readonly>
							<?php endif; ?>
						</div>
					</div>
					<div class="item form-group">
						<label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Alamat <span class="required">*</span></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea id="alamat" name="address" class="form-control col-md-7 col-xs-12" <?= $operation == 'view' ? 'readonly' : '';?>><?= isset($inputs['address']) ? $inputs['address'] : '' ?></textarea>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="other_info">Deskripsi
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea id="keterangan" name="other_info" class="form-control col-md-7 col-xs-12" <?= $operation == 'view' ? 'readonly' : '';?>><?= isset($inputs['other_info']) ? $inputs['other_info'] : '' ?></textarea>
						</div>
					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-3">
							<a href="<?= base_url('backoffice/properti'); ?>" class="btn btn-primary"><?= $operation == 'view' ? 'Kembali' : 'Cancel' ?></a>
							<?php if ($operation == 'new' || $operation == 'edit'): ?><button id="send" type="submit" class="btn btn-success">Simpan</button><?php endif; ?>
						</div>
					</div>
				<?= form_close() ?>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>
