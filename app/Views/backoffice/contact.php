<?= $this->extend('backoffice/partials/default') ?>
<?= $this->section('content') ?>
<div class="x_panel">
	<?php
	$inputx = session()->getFlashdata('inputx');
	$errors = session()->getFlashdata('errors');
	$success = session()->getFlashdata('success');
	if (!empty($errors)) : ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= esc($error) ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endif;
	if (!empty($success)) : ?>
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<?= $success ?>
		</div>
	<?php endif; ?>
	<div class="x_title">
		<h2>Edit Kontak</h2>
		<div class="clearfix"></div>
	</div>
	<div class="x_content">

		<form class="form-horizontal form-label-left" action="<?= base_url('backoffice/contact/update') ?>" method="post">

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="whatsapp">Whatsapp <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input id="whatsapp" class="form-control col-md-7 col-xs-12" name="whatsapp" type="text" value="<?= $inputx['whatsapp'] ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input id="email" class="form-control col-md-7 col-xs-12" name="email" type="email" value="<?= $inputx['email'] ?>">
				</div>
			</div>
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="instagram">Instagram <span class="required">*</span>
				</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input id="instagram" class="form-control col-md-7 col-xs-12" name="instagram" type="text" value="<?= $inputx['instagram'] ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-md-offset-3">
					<a href="<?= base_url('backoffice') ?>" class="btn btn-primary">Cancel</a>
					<button type="submit" class="btn btn-success">Save</button>
				</div>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection() ?>
