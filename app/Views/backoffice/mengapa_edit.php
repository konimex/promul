<?= $this->extend('backoffice/partials/default') ?>
<?= $this->section('content') ?>
<div class="x_panel">
	<?php
	$inputs = session()->getFlashdata('inputs');
	$errors = session()->getFlashdata('errors');
	$success = session()->getFlashdata('success');
	if (!empty($errors)) : ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= esc($error) ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endif;
	if (!empty($success)) : ?>
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<?= $success ?>
		</div>
	<?php endif; ?>
	<div class="x_title">
		<h2>Edit Informasi</h2>
		<div class="clearfix"></div>
	</div>
	<div class="x_content">
		<?php $attributes = ['class' => 'form-horizontal form-label-left', 'id' => 'divTestimoni'];
		if ($operation == 'new') : ?>
			<?= form_open_multipart(base_url('backoffice/mengapa/create'), $attributes); ?>
		<?php elseif ($operation == 'edit') : ?>
			<?= form_open_multipart(base_url('backoffice/mengapa/update/' . $id), $attributes); ?>
		<?php endif; ?>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="icon">Foto
			</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input id="icon" name="icon" type="file" <?= $operation == 'new' ? 'required' : '' ?>>
			</div>
		</div>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Judul <span class="required">*</span>
			</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input class="form-control col-md-7 col-xs-12" name="title" id="fullname" value="<?= isset($inputs['title']) ? $inputs['title'] : '' ?>">
			</div>
		</div>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Deskripsi <span class="required">*</span>
			</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<textarea class="form-control col-md-7 col-xs-12" name="description" id="description"><?= isset($inputs['description']) ? $inputs['description'] : '' ?></textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-9 col-md-offset-3">
				<a href="<?= base_url('backoffice/mengapa') ?>" class="btn btn-primary">Cancel</a>
				<button type="submit" class="btn btn-success">Save</button>
			</div>
		</div>
		<?= form_close() ?>
	</div>
</div>
<?= $this->endSection() ?>
