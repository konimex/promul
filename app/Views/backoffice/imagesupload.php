<?= $this->extend('backoffice/partials/default') ?>

<?= $this->section('content') ?>
<div class="col-md-12 col-sm-12 col-xs-12">
	<?php
	$errors = session()->getFlashdata('errors');
	if (!empty($errors)) : ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<ul>
				<?php foreach ($errors as $error) : ?>
					<li><?= $error ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endif; ?>
	<div class="x_panel">
		<div class="x_title">
				<h2><?php if ($operation == 'new'): ?>Tambah<?php elseif ($operation == 'edit'): ?>Edit<?php else: ?>Lihat<?php endif;?> Gambar Properti <small><?= $name ?></small></h2>
			<div class="clearfix"></div>
		</div>
		<div>
			<div class="x_content">
				<?php if ($operation == 'new') : ?>
					<?= form_open_multipart(base_url('backoffice/uploads/create/' . $id_properti)); ?>
				<?php elseif ($operation == 'edit') : ?>
					<?= form_open_multipart(base_url('backoffice/uploads/update/' . $id)); ?>
				<?php endif; ?>
				<?php if ($operation == 'view' || $operation == 'preview'): ?>
				<div class="form-group">
					<img src="<?= base_url('uploads/properti/' . $id_properti . '/' . $img_name) ?>" />
				</div>
				<?php else: ?>
				<div class="form-group">
					<label for="gambar">File </label>
					<input type="file" <?= $operation == 'new' ? 'name="gambar[]" multiple' : 'name="gambar"' ?>>
				</div>
				<?php endif; ?>
				<br>
				<div class="form-group">
					<?php if ($operation == 'view'): ?>
						<a href="<?= base_url('backoffice/uploads/view/' . $id_properti) ?>" class="btn btn-primary">Kembali</a>
					<?php else: ?>
					<div>
						<a href="<?= base_url('backoffice/uploads/edit/' . $id_properti) ?>" class="btn btn-primary">Kembali</a>
						<?php if (!($operation == 'preview')): ?>
						<button type="submit" class="btn btn-success">Upload</button>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
				<?= form_close() ?>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>
