						<!-- menu profile quick info -->
						<div class="profile clearfix">
							<div class="profile_pic">
								<img src="<?php echo base_url('backoffice-assets/prod/images/img.jpg'); ?>" alt="..." class="img-circle profile_img">
							</div>
							<div class="profile_info">
								<span>Welcome,</span>
								<h2><?= session()->get('username') ?></h2>
							</div>
						</div>
						<!-- /menu profile quick info -->
						<br />
						<!-- sidebar menu -->
						<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
							<div class="menu_section">
								<ul class="nav side-menu">
									<li><a href="<?= base_url('backoffice') ?>"><i class="fa fa-desktop"></i> Dashboard</a></li>
									<li><a href="<?= base_url('backoffice/slider') ?>"><i class="fa fa-files-o"></i> Slider</a></li>
									<li><a href="<?= base_url('backoffice/mengapa') ?>"><i class="fa fa-info-circle"></i> Edit Promosi</a></li>
									<li><a href="<?= base_url('backoffice/contact') ?>"><i class="fa fa-user"></i> Edit Kontak</a></li>
								</ul>
								<h3>Testimoni</h3>
								<ul class="nav side-menu">
									<li><a href="<?= base_url('backoffice/testimoni') ?>"><i class="fa fa-thumbs-up"></i> List Testimoni</a></li>
									<li><a href="<?= base_url('backoffice/testimoni/add_new') ?>"><i class="fa fa-comments"></i> Tambah Testimoni</a></li>
								</ul>
								<h3>Properti</h3>
								<ul class="nav side-menu">
									<li><a href="<?= base_url('backoffice/properti') ?>"><i class="fa fa-wrench"></i> All Properties</a></li>
									<li><a href="<?= base_url('backoffice/properti/top_properti') ?>"><i class="fa fa-star"></i> Top Property</a></li>
									<li><a href="<?= base_url('backoffice/properti/add_new') ?>"><i class="fa fa-home"></i> Add New Property</a></li>
								</ul>
							</div>
						</div>
						<!-- /sidebar menu -->
					</div>
				</div>
