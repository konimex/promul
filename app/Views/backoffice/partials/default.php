<?= $this->include('backoffice/partials/header') ?>
<?= $this->include('backoffice/partials/sidebar') ?>
<?= $this->include('backoffice/partials/topnav') ?>
<div class="right_col" role="main" style="min-height: 100vh;">
<?= $this->renderSection('content') ?>
</div>
<?= $this->include('backoffice/partials/footer') ?>
