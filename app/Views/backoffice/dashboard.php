<?= $this->extend('backoffice/partials/default') ?>

<?= $this->section('content') ?>
<?php $success = session()->getFlashdata('success');
if (!empty($success)) : ?>
	<div class="row">
		<div class="alert alert-success alert-dismissible " role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
			</button>
			<?= $success ?>
		</div>
	</div>

	<style>

	.tabel-datatable2{
	clear: both;
    margin-top: 6px !important;
    margin-bottom: 6px !important;
    max-width: none !important;
    border-collapse: separate !important;
}
	</style>
<?php endif; ?>
<!-- <div class="row top_tiles">
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="tile-stats">
			<div class="count">179</div>
			<h2 style="margin-left: 10px">Total Visitor</h2>
		</div>
	</div>
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="tile-stats">
			<div class="count"><?= $jumlahKontrakan ?></div>
			<h2 style="margin-left: 10px">Total Kontrakan</h2>
		</div>
	</div>
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="tile-stats">
			<div class="count"><?= $jumlahVilla ?></div>
			<h2 style="margin-left: 10px">Total Villa</h2>
		</div>
	</div>
	<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-6">
		<div class="tile-stats">
			<div class="count"><?= $jumlahKos ?></div>
			<h2 style="margin-left: 10px">Total Kosan</h2>
		</div>
	</div>
</div> -->
<div class="row tile_count">
	<div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
		<a href="<?= base_url('backoffice') ?>"><span class="count_top"><i class="fa fa-user"></i> Total Visitor</span>
		<div class="count"><?= $jumlahVisitor ?> </div>
		<span class="count_bottom"><i class="green">Keseluruhan </i> Data</span></a>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
		<a href="<?= base_url('backoffice/properti') ?>"><span class="count_top"><i class="fa fa-user"></i> Total Kontrakan</span>
		<div class="count"><?= $jumlahKontrakan ?></div>
		<span class="count_bottom"><i class="green">Keseluruhan </i> Data</span></a>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
		<a href="<?= base_url('backoffice/properti') ?>"><span class="count_top"><i class="fa fa-user"></i> Total Kos</span>
		<div class="count"><?= $jumlahKos ?></div>
		<span class="count_bottom"><i class="green">Keseluruhan </i> Data</span></a>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-3 tile_stats_count">
		<a href="<?= base_url('backoffice/properti') ?>"><span class="count_top"><i class="fa fa-user"></i> Total Vila</span>
		<div class="count"><?= $jumlahVilla ?></div>
		<span class="count_bottom"><i class="green">Keseluruhan </i> Data</span></a>
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tabel Kosan</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
					<div class="row">
						<div class="col-sm-12">
							<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
								<thead>
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 192px;" aria-sort="ascending" aria-label="No: activate to sort column descending">No</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 452px;" aria-label="Nama Kosan: activate to sort column ascending">Nama Kosan</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 436px;" aria-label="Jenis Kosan: activate to sort column ascending">Jenis Kosan</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 350px;" aria-label="Status: activate to sort column ascending">Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $number = 0 ?>
									<?php foreach ($kos as $row) : ?>
										<?php $number += 1 ?>
										<tr role="row" class="<?= $number % 2 ? 'odd' : 'even' ?>">
											<td class="sorting_1"><?= $number ?></td>
											<td><?= $row['name'] ?></td>
											<td><?= $row['occupant_type'] ?></td>
											<td><?= $row['available'] ? 'Tersedia' : 'Penuh' ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tabel Kontrakan</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div id="datatable-checkbox_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
					<div class="row">
						<div class="col-sm-12">
							<table id="datatable-checkbox" class="table table-striped table-bordered bulk_action dataTable no-footer" role="grid" aria-describedby="datatable-checkbox_info">
								<thead>
									<tr role="row">
										<th class="sorting_disabled" rowspan="1" colspan="1" style="width: 225px;" aria-label="Nomor">Nomor</th>
										<th class="sorting_asc" tabindex="0" aria-controls="datatable-checkbox" rowspan="1" colspan="1" style="width: 448px;" aria-sort="ascending" aria-label="Nama Kosan: activate to sort column descending">Nama Kosan</th>
										<th class="sorting" tabindex="0" aria-controls="datatable-checkbox" rowspan="1" colspan="1" style="width: 432px;" aria-label="Jenis Kosan: activate to sort column ascending">Jenis Kosan</th>
										<th class="sorting" tabindex="0" aria-controls="datatable-checkbox" rowspan="1" colspan="1" style="width: 347px;" aria-label="Status: activate to sort column ascending">Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $number = 0 ?>
									<?php foreach ($kontrakan as $row) : ?>
										<?php $number += 1 ?>
										<tr role="row" class="<?= $number % 2 ? 'odd' : 'even' ?>">
											<td class="sorting_1"><?= $number ?></td>
											<td><?= $row['name'] ?></td>
											<td><?= $row['occupant_type'] ?></td>
											<td><?= $row['available'] ? 'Tersedia' : 'Penuh' ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Tabel Villa</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div id="datatable_wrapper2" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
					<div class="row">
						<div class="col-sm-12">
							<table id="datatable-fixed-header" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
								<thead>
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 192px;" aria-sort="ascending" aria-label="No: activate to sort column descending">No</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 452px;" aria-label="Nama Kosan: activate to sort column ascending">Nama Kosan</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 436px;" aria-label="Jenis Kosan: activate to sort column ascending">Jenis Kosan</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 350px;" aria-label="Status: activate to sort column ascending">Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $number = 0 ?>
									<?php foreach ($villa as $row) : ?>
										<?php $number += 1 ?>
										<tr role="row" class="<?= $number % 2 ? 'odd' : 'even' ?>">
											<td class="sorting_1"><?= $number ?></td>
											<td><?= $row['name'] ?></td>
											<td><?= $row['occupant_type'] ?></td>
											<td><?= $row['available'] ? 'Tersedia' : 'Penuh' ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>
