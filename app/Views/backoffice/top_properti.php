<?= $this->extend('backoffice/partials/default') ?>

<?= $this->section('content') ?>

<style>

	.star i.fa-star{
		color:#ffff00;
	}
</style>
<div class="row">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Top Properti</h2>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
					<div class="row">
						<div class="col-sm-12">
							<?php $success = session()->getFlashdata('success');
							if(!empty($success)) :?>
							<div class="alert alert-success alert-dismissible " role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
								</button>
								<?= $success ?>
							</div>
							<?php endif; ?>
							<table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
								<thead>
									<tr role="row">
										<th class="sorting_asc" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 5%;" aria-sort="ascending" aria-label="No: activate to sort column descending">No</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 412px;" aria-label="Nama: activate to sort column ascending">Nama Kosan</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 412px;" aria-label="Tipe Properti: activate to sort column ascending">Tipe Properti</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 310px;" aria-label="Harga: activate to sort column ascending">Harga</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 404px;" aria-label="Status: activate to sort column ascending">Status</th>
										<th class="sorting" tabindex="0" aria-controls="datatable" rowspan="1" colspan="1" style="width: 237px;" aria-label="Action: activate to sort column ascending">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $number = 0 ?>
									<?php foreach ($properti as $row) : ?>
										<?php $number += 1 ?>
										<tr role="row" class="<?= $number % 2 ? 'odd' : 'even' ?>">
											<th scope="row" class="sorting_1"><?= $number ?></th>
											<td><?= $row['name'] ?></td>
											<td><?= ucfirst($row['tipe_properti']) ?></td>
											<td><?php $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY);
												$fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
												echo numfmt_format_currency($fmt, $row['price'], "IDR"); ?></td>
											<td><?php if (!$row['available']) echo 'Penuh';
												else echo 'Tersedia'; ?></td>
											<td><a href="<?php echo base_url('/backoffice/properti/view/' . $row['id']); ?>"><i class="fa fa-search"></i></a> <a href="<?php echo base_url('/backoffice/properti/edit/' . $row['id']); ?>"><i class="fa fa-pencil"></i></a><a href="#" data-toggle="modal" data-target="#modal-danger<?= $row['id'] ?>"><i class="fa fa-times" style="margin-left:3px ;"></i></a></td>
										</tr>
										<div class="modal fade bs-example-modal-lg" id="modal-danger<?= $row['id'] ?>" tabindex="-1" role="dialog" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title" id="myModalLabel">Peringatan</h4>
													</div>
													<div class="modal-body">
														<h4>Anda akan menghapus <strong><?= $row['name'] ?></strong> dari Top Property</h4>
														<p>Apakah Anda yakin akan menghapus properti tersebut dari Top Property? Tindakan ini tidak akan menghapus properti yang dibuat.</p>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Batalkan</button>
														<a class="btn btn-danger" href="<?= base_url('/backoffice/properti/del_top/' . $row['id']) ?>">Hapus</a>
													</div>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
   $(".like").click(function(){
	   $(this).toggleClass("star")
   });
 });
</script>
<?= $this->endSection() ?>
