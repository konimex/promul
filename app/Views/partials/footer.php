<?php 
$contactWA = '';
$contactEmail = '';
$contactInstagram = '';
foreach($contact as $row) {
    $contactWA = $row['whatsapp'];
    $contactEmail = $row['email'];
    $contactInstagram = $row['instagram'];
} 
?>
    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="fs-about">
                        <div class="fs-logo">
                            <a href="<?= base_url('home') ?>">
                                <img src="<?php echo base_url('frontoffice-assets/img/f-logoori.png'); ?>" alt="">
                            </a>
                        </div>
                        <p>Cari Kos di Pertamina siap membantu mahasiswa mahasiswi Universitas Pertamina dalam memenuhi kebutuhan tempat tinggal dengan menyediakan informasi hunian di sekitar Universitas Pertamina</p>
                        <div class="fs-social">
                            <a target="_blank" href="https://www.instagram.com/<?= $contactInstagram ?>/"><img src="<?php echo base_url('frontoffice-assets/img/icon/tik-tok3.png'); ?>" alt=""></a>
                            <a target="_blank" href="https://www.instagram.com/<?= $contactInstagram ?>/"><i class="fa fa-instagram"></i></a>
                            <a target="_blank" href="https://timeline.line.me/user/_dbhx4fm7Bm670HXiItv2VJBxYXufWlaRgKvGLs0?utm_medium=windows&utm_source=desktop&utm_campaign=OA_Profile"><img src="<?php echo base_url('frontoffice-assets/img/icon/line3.png'); ?>" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="fs-widget">
                        <h5>INFORMASI</h5>
                        <ul>
                            <li><a href="<?= base_url('property') ?>">All Properti</a></li>
                            <li><a href="<?= base_url('property/vila') ?>">Vila</a></li>
                            <li><a href="<?= base_url('property/kontrakan') ?>">Kontrakan</a></li>
                            <li><a href="<?= base_url('property/kos') ?>">Kos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="fs-widget">
                        <h5>HUBUNGI KAMI</h5>
                        <ul>
                            <li><a href="mailto:<?= $contactEmail ?>?subject=Mail from Website carikosdipertamina.com">Email Us</a></li>
                            <li><a target="_blank" href="https://web.whatsapp.com/send?phone=<?= $contactWA?> CariKos di Pertamina&amp;text=Halo, Saya mau bertanya info hunian sekitar Universitas Pertamina.">WhatsApp</a></li>
                            <li><a href="<?= base_url('about/#OurTeam') ?>">Our Team</a></li>
                            <li><a href="<?= base_url('about') ?>">About Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="fs-widget">
                        <h5>Update Hunian</h5>
                        <p>Tidak ingin ketinggalan informasi hunian terbaru dari CariKos di Pertamina. Subscribe Us!</p>
                        <form action="#" class="subscribe-form">
                            <input type="text" placeholder="Email">
                            <button type="submit" class="site-btn">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="copyright-text">
                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="<?php echo base_url('frontoffice-assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/jquery.magnific-popup.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/mixitup.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/jquery.nice-select.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/jquery.slicknav.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/jquery.richtext.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/image-uploader.min.js'); ?>"></script>
    <script src="<?php echo base_url('frontoffice-assets/js/main.js'); ?>"></script>
</body>

</html>