<?php 
$contactWA = '';
$contactEmail = '';
$contactInstagram = '';
foreach($contact as $row) {
    $contactWA = $row['whatsapp'];
    $contactEmail = $row['email'];
    $contactInstagram = $row['instagram'];
} 
?>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Wrapper Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <div class="canvas-close">
            <span class="icon_close"></span>
        </div>
        <div class="logo">
            <a href="<?= base_url('home') ?>">
                <img src="<?php echo base_url('frontoffice-assets/img/logo_edited.png'); ?>" alt="">
            </a>
        </div>
        <div id="mobile-menu-wrap"></div>
        <div class="om-widget">
            <ul>
                <a href="mailto:<?= $contactEmail ?>?subject=Mail from Website carikosdipertamina.com"><li><i class="icon_mail_alt"></i> <?= $contactEmail ?></li></a>
                <a target="_blank" href="https://web.whatsapp.com/send?phone=<?= $contactWA ?> CariKos di Pertamina&amp;text=Halo, Saya mau bertanya info hunian sekitar Universitas Pertamina."><li><i class="fa fa-mobile-phone"></i> <?= $contactWA ?> </li></a>
            </ul>
            <a href="<?= base_url('property') ?>" class="hw-btn">View All Property</a>
        </div>
        <div class="om-social">
            <a href="#"><img src="<?php echo base_url('frontoffice-assets/img/icon/tik-tok.png'); ?>" alt=""></a>
            <a target="_blank" href="https://www.instagram.com/<?= $contactInstagram ?>/"><i class="fa fa-instagram"></i></a>
            <a target="_blank" href="https://timeline.line.me/user/_dbhx4fm7Bm670HXiItv2VJBxYXufWlaRgKvGLs0?utm_medium=windows&utm_source=desktop&utm_campaign=OA_Profile"><img src="<?php echo base_url('frontoffice-assets/img/icon/line.png'); ?>" alt=""></a>
            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-youtube-play"></i></a>
            <a href="#"><i class="fa fa-instagram"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a> -->
        </div>
    </div>
    <!-- Offcanvas Menu Wrapper End -->

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="hs-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="logo">
                            <a href="<?= base_url('home') ?>"><img src="<?php echo base_url('frontoffice-assets/img/logo_edited.png'); ?>" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="ht-widget" style="padding-top:40px;">
                            <ul>
                                <a target="_blank" href="mailto:<?= $contactEmail ?>?subject=Mail from Website carikosdipertamina.com"><li><i class="icon_mail_alt"></i> <?= $contactEmail ?></li></a>
                                <a target="_blank" href="https://web.whatsapp.com/send?phone=<?= $contactWA ?>&amp;text=Halo CariKos di Pertamina, Saya mau bertanya info hunian sekitar Universitas Pertamina."><li><i class="fa fa-mobile-phone"></i> <?= $contactWA ?> </li></a>
                            </ul>
                            <a href="<?= base_url('property') ?>" class="hw-btn">View All Property</a>
                        </div>
                    </div>
                </div>
                <div class="canvas-open" style="margin-top:20px; margin-right: 15px;">
                    <span class="icon_menu"></span>
                </div>
            </div>
        </div>
        <div class="hs-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <nav class="nav-menu">
                            <ul>
                                <li <?php if($topnav=="Home") echo "class='active'" ?>><a href="<?= base_url('home') ?>">Home</a></li>
                                <li <?php if($topnav=="Vila") echo "class='active'" ?> ><a href="<?= base_url('property/vila') ?>">Vila</a>
                                    <!-- <ul class="dropdown">
                                        <li><a href="./property.html">Property Grid</a></li>
                                        <li><a href="./profile.html">Property List</a></li>
                                        <li><a href="./property-details.html">Property Detail</a></li>
                                        <li><a href="./property-comparison.html">Property Comperison</a></li>
                                        <li><a href="./property-submit.html">Property Submit</a></li>
                                    </ul> -->
                                </li>
                                <li <?php if($topnav=="Kontrakan") echo "class='active'" ?>><a href="<?= base_url('property/kontrakan') ?>">Kontrakan</a></li>
                                <li <?php if($topnav=="Kos") echo "class='active'" ?>><a href="<?= base_url('property/kos') ?>">Kos</a></li>
                                <li <?php if($topnav=="About") echo "class='active'" ?>><a href="<?= base_url('about') ?>">About Us</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3">
                        <div class="hn-social">
                            <a href="#"><img src="<?php echo base_url('frontoffice-assets/img/icon/tik-tok.png'); ?>" alt=""></a>
                            <a target="_blank" href="https://www.instagram.com/<?= $contactInstagram ?>/"><i class="fa fa-instagram"></i></a>
                            <a target="_blank" href="https://timeline.line.me/user/_dbhx4fm7Bm670HXiItv2VJBxYXufWlaRgKvGLs0?utm_medium=windows&utm_source=desktop&utm_campaign=OA_Profile"><img src="<?php echo base_url('frontoffice-assets/img/icon/line.png'); ?>" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->
