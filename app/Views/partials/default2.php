<?= $this->include('partials/header') ?>
<?= $this->include('partials/topnav') ?>
<!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section spad set-bg" data-setbg="<?php echo base_url('frontoffice-assets/img/breadcrumb-bg.jpg'); ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text">
                        <h4><?= $headerTitle ?> Property</h4>
                        <div class="bt-option">
                            <a href="<?= base_url('home') ?>"><i class="fa fa-home"></i> Home</a>
                            <span><?= $headerTitle ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->
<?= $this->renderSection('content') ?>

<?= $this->include('partials/footer') ?>
