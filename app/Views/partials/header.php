<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Aler Template">
    <meta name="keywords" content="Aler, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title ?></title>
    <link rel="icon" href="<?php echo base_url('frontoffice-assets/img/f-logoori.png'); ?>" type="image/icon type">
        
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link href="<?php echo base_url('frontoffice-assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontoffice-assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontoffice-assets/css/elegant-icons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontoffice-assets/css/jquery-ui.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontoffice-assets/css/nice-select.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontoffice-assets/css/owl.carousel.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontoffice-assets/css/magnific-popup.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontoffice-assets/css/slicknav.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('frontoffice-assets/css/style.css'); ?>" rel="stylesheet">

     
</head>