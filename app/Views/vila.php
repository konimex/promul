<?= $this->extend('partials/default2') ?>

<?= $this->section('content') ?>

    <!-- Search Section Begin -->
    <section class="search-section spad">
        <form method="post" action="<?= base_url('property/search') ?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="section-title">
                        <h4>Filter Vila</h4>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="change-btn">
                        <div class="cb-item">
                            <label for="tersedia" class="active">
                                Tersedia
                                <input name="available" type="radio" id="tersedia" value="1">
                            </label>
                        </div>
                        <div class="cb-item">
                            <label for="penuh">
                                Penuh
                                <input name="available" type="radio" id="penuh" value="0">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search-form-content">  
                <div class="filter-form"> 
                    <select name="tipe_properti" class="sm-width">
                        <option disabled>Pilih Property</option>
                        <option selected value="Villa">Vila</option>
                        <option value="Kontrakan">Kontrakan</option>
                        <option value="Kos">Kos</option>
                    </select>
                    <select name="occupant_type" class="sm-width">
                        <option selected disabled>Pilih Jenis Penghuni</option>
                        <option value="Putra">Laki-laki</option>
                        <option value="Putri">Perempuan</option>
                        <option value="Bebas">Campur</option>
                    </select>
                    <div class="price-range-wrap sm-width">
                        <div class="price-text">
                            <label for="priceRange">Price: </label>
                            <input type="text" id="priceRange" name="priceRange" readonly style="width: 200px;">
                        </div>
                        <div id="price-range" class="slider"></div>
                    </div>
                    <input class="xl-width" style="height: 46px; padding: .375rem .75rem; border: 1px solid #ced4da; transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out; margin-right: 20px; font-family: inherit; font-size: 14px; font-weight: normal;" placeholder="Cari lokasi" name="keyword"/>
                    <button type="submit" class="search-btn sm-width">Search</button>
                </div>
            </div>
            <div class="more-option">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-heading active">
                            <a data-toggle="collapse" data-target="#collapseOne">
                                Fasilitas Property
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="mo-list">
                                    <div class="ml-column">
                                        <label for="ac">Air conditioning
                                            <input name="ac" type="checkbox" id="ac" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                        <label for="wifi">Wifi
                                            <input name="internet" type="checkbox" id="wifi" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                    </div>
                                    <div class="ml-column">
                                        <label for="almari">Almari
                                            <input name="lemari" type="checkbox" id="almari" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                        <label for="kamar_mandi">Kamar Mandi
                                            <input name="kamarmandi" type="checkbox" id="kamar_mandi" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                    </div>
                                    <div class="ml-column">
                                        <label for="kasur">Kasur
                                            <input name="kasur" type="checkbox" id="kasur" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                        <label for="dapur_bersama">Dapur Bersama
                                            <input name="dapur_bersama" type="checkbox" id="dapur_bersama" value="1">
                                            <span class="checkbox"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </section>
    <!-- Search Section End -->

    <!-- Property Section Begin -->
    <section class="property-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h4>VILA PROPERTY</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach($properti as $latestPrpt): ?>
                <?php if($latestPrpt['tipe_properti'] == 'Villa'): ?>
                <div class="col-lg-4 col-md-6">
                    <div class="property-item">
                        <?php 
                        $imgProperti = "";
                        foreach($propertiImg as $prptImg) {
                            if ($latestPrpt['id'] == $prptImg['id_properti']) {
                                $imgProperti = $prptImg['img_name'];
                                break;
                            }
                        } ?>
                        <div class="pi-pic set-bg" data-setbg="<?php echo base_url('uploads/properti/'.$latestPrpt['id'].'/'.$imgProperti); ?>">
                            <div class="<?php if($latestPrpt['tipe_properti'] == 'Villa') {echo 'label';} elseif($latestPrpt['tipe_properti'] == 'Kos') {echo 'label c-red';} elseif($latestPrpt['tipe_properti'] == 'Kontrakan') {echo 'label c-magenta';} ?>"><?php if($latestPrpt['tipe_properti'] == 'Villa') {echo 'Vila';} elseif($latestPrpt['tipe_properti'] == 'Kos') {echo 'Kos';} elseif($latestPrpt['tipe_properti'] == 'Kontrakan') {echo 'Kontrakan';} ?></div>
                        </div>
                        <div class="pi-text">
                            <div class="pt-price">
                                <?php 
                                    $fmt = new NumberFormatter('id_ID', NumberFormatter::CURRENCY);
                                    $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 0);
                                    echo numfmt_format_currency($fmt, $latestPrpt['price'], "IDR");
                                    ?>        
                            </div>
                            <?php $char = strlen($latestPrpt['name']);
                            if($char < 27) : ?>
                            <h5><a href="<?= base_url('property/detail/'.$latestPrpt['tipe_properti'].'/'.$latestPrpt['id']) ?>"><?= $latestPrpt['name'] ?></a></h5>
                            <?php else: ?>
                                <?php
                                $words = explode(" ",$latestPrpt['name']);
                                $word = implode(" ",array_splice($words,0,4));
                                ?>
                            <h5><a href="<?= base_url('property/detail/'.$latestPrpt['tipe_properti'].'/'.$latestPrpt['id']) ?>"><?= $word ?> ...</a></h5>    
                            <?php endif; ?>
                            <p><span class="icon_pin_alt"></span> <?= $latestPrpt['address'] ?></p>
                            <ul>
                                <li><i class="fa fa-wifi"></i> <?php if($latestPrpt['internet'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-bathtub"></i> <?php if($latestPrpt['kamarmandi'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-bed"></i> <?php if($latestPrpt['kasur'] == 1) echo "Yes"; else echo "No"; ?></li>
                                <li><i class="fa fa-spinner"></i> <?php if($latestPrpt['ac'] == 1) echo "Yes"; else echo "No"; ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <!-- Property Section End -->

<?= $this->endSection() ?>