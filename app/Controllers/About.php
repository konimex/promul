<?php namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Mengapa_model;
use App\Models\Testimoni_model;
use App\Models\Contact_model;

class About extends Controller
{

	public function __construct()
	{
		helper('form');
		helper('filesystem');

		$this->mengapa_model = new Mengapa_model();

		$this->testimoni_model = new Testimoni_model();

		$this->contact_model = new Contact_model();

		$this->form_validation = \Config\Services::validation();
		$this->session = session();
	}

	public function index()
	{
		$data['title'] = "About Us | CariKos di Pertamina";
		$data['headerTitle'] = "About Us ";
		$data['topnav'] = "About";

		$data['mengapa'] = $this->mengapa_model->readMengapa();
		$data['testimoni'] = $this->testimoni_model->readTestimoni();
		$data['contact'] = $this->contact_model->readContact();

		return view('about', $data);
	}

	//--------------------------------------------------------------------

}
