<?php namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Top_model;
use App\Models\Properti_model;
use App\Models\PropertiImg_model;
use App\Models\Mengapa_model;
use App\Models\Testimoni_model;
use App\Models\Visitor_model;
use App\Models\Contact_model;

class Home extends Controller
{
	public function __construct()
	{
		helper('form');
		helper('filesystem');

		$this->visitor_model = new Visitor_model();

		$this->slider_model = new Top_model();

		$this->properti_model = new Properti_model();
		$this->img_model = new PropertiImg_model();

		$this->mengapa_model = new Mengapa_model();

		$this->testimoni_model = new Testimoni_model();

		$this->contact_model = new Contact_model();

		$this->form_validation = \Config\Services::validation();
		$this->session = session();

	}

	public function index()
	{
		// Visitor
		if (isset($_SESSION['visitor'])) {
	    	//condition to calculate one visitor
	    	$_SESSION['visitor'] = 'user';
	    } else {
	    	//Create session to calculate total visitor
	    	$_SESSION['visitor'] = 'user';

	    	// Get ip user
		    $ipUser = ' - ';
		    if (getenv('HTTP_CLIENT_IP'))
		        $ipUser = getenv('HTTP_CLIENT_IP');
		    else if(getenv('HTTP_X_FORWARDED_FOR'))
		        $ipUser = getenv('HTTP_X_FORWARDED_FOR');
		    else if(getenv('HTTP_X_FORWARDED'))
		        $ipUser = getenv('HTTP_X_FORWARDED');
		    else if(getenv('HTTP_FORWARDED_FOR'))
		        $ipUser = getenv('HTTP_FORWARDED_FOR');
		    else if(getenv('HTTP_FORWARDED'))
		       $ipUser = getenv('HTTP_FORWARDED');
		    else if(getenv('REMOTE_ADDR'))
		        $ipUser = getenv('REMOTE_ADDR');
		    else
		        $ipUser = 'UNKNOWN';
		    // End Get ip user

		    // Get OS user
		    $user_agent = $_SERVER['HTTP_USER_AGENT'];
		    $userOS  = "Unknown OS Platform";
		    $os_array     = array(
	              '/windows nt 10/i'      =>  'Windows 10',
	              '/windows nt 6.3/i'     =>  'Windows 8.1',
	              '/windows nt 6.2/i'     =>  'Windows 8',
	              '/windows nt 6.1/i'     =>  'Windows 7',
	              '/windows nt 6.0/i'     =>  'Windows Vista',
	              '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
	              '/windows nt 5.1/i'     =>  'Windows XP',
	              '/windows xp/i'         =>  'Windows XP',
	              '/windows nt 5.0/i'     =>  'Windows 2000',
	              '/windows me/i'         =>  'Windows ME',
	              '/win98/i'              =>  'Windows 98',
	              '/win95/i'              =>  'Windows 95',
	              '/win16/i'              =>  'Windows 3.11',
	              '/macintosh|mac os x/i' =>  'Mac OS X',
	              '/mac_powerpc/i'        =>  'Mac OS 9',
	              '/linux/i'              =>  'Linux',
	              '/ubuntu/i'             =>  'Ubuntu',
	              '/iphone/i'             =>  'iPhone',
	              '/ipod/i'               =>  'iPod',
	              '/ipad/i'               =>  'iPad',
	              '/android/i'            =>  'Android',
	              '/blackberry/i'         =>  'BlackBerry',
	              '/webos/i'              =>  'Mobile'
	        );
		    foreach ($os_array as $regex => $value)
		        if (preg_match($regex, $user_agent))
		            $userOS = $value;
		    // End Get OS User

		    // Insert to database
		    $currentTime = new Time('now', 'Asia/Jakarta', 'en_US');
			$data = [
				'os'			=> $userOS,
				'ip'			=> $ipUser,
				'creation_date'	=> $currentTime,
			];
			$this->visitor_model->createVisitor($data);
			// End Insert to database

	    }


		$data['topnav'] = "Home";
		$data['title'] = "Home | CariKos di Pertamina";

		$data['slider'] = $this->slider_model->readTop();
		$data['properti'] = $this->properti_model->readOrderedProperti();
		$data['propertiImg'] = $this->img_model->readImgList();
		$data['mengapa'] = $this->mengapa_model->readMengapa();
		$data['testimoni'] = $this->testimoni_model->readTestimoni();
		$data['contact'] = $this->contact_model->readContact();

		$data['kos'] = $this->properti_model->getWhere(['tipe_properti' => 'Kos'])->getResultArray();
		$data['kontrakan'] = $this->properti_model->getWhere(['tipe_properti' => 'Kontrakan'])->getResultArray();
		$data['villa'] = $this->properti_model->getWhere(['tipe_properti' => 'Villa'])->getResultArray();
		$data['jumlahKos'] = $this->properti_model->where(['tipe_properti' => 'Kos'])->countAllResults();
		$data['jumlahKontrakan'] = $this->properti_model->where(['tipe_properti' => 'Kontrakan'])->countAllResults();
		$data['jumlahVilla'] = $this->properti_model->where(['tipe_properti' => 'Villa'])->countAllResults();

		return view('home', $data);
	}

	//--------------------------------------------------------------------

}
