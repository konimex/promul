<?php namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Properti_model;
use App\Models\PropertiImg_model;
use App\Models\Contact_model;

class Property extends Controller
{
	public function __construct()
	{
		helper('form');
		helper('filesystem');

		$this->properti_model = new Properti_model();
		$this->img_model = new PropertiImg_model();

		$this->contact_model = new Contact_model();

		$this->form_validation = \Config\Services::validation();
		$this->session = session();
	}

	public function index()
	{
		$data['title'] = "All Property | CariKos di Pertamina";
		$data['headerTitle'] = "All ";

		// $data['properti'] = $this->properti_model->readProperti();
		$data['propertiImg'] = $this->img_model->readImgList();
		$data['contact'] = $this->contact_model->readContact();

		$data['properti'] = $this->properti_model->paginate(9, 'properti');
		$data['pager'] = $this->properti_model->pager;

		return view('allproperty', $data);
	}

	public function search()
	{
		$data['title'] = "Search Property | CariKos di Pertamina";
		$data['headerTitle'] = "Search ";
		$data['topnav'] = "";
		$keyword = $this->request->getPost('keyword');
		// $pricelow = 0; // placeholder
		// $pricehigh = 500000000; // placeholder
		$priceRange = $this->request->getPost('priceRange');
		$tipe_properti = $this->request->getPost('tipe_properti');
		$occupant_type = $this->request->getPost('occupant_type');
		$dapur_bersama = $this->request->getPost('dapur_bersama');
		$ac = $this->request->getPost('ac');
		$internet = $this->request->getPost('internet');
		$lemari = $this->request->getPost('lemari');
		$kasur = $this->request->getPost('kasur');
		$kamarmandi = $this->request->getPost('kamarmandi');
		$available = $this->request->getPost('available');
		if ($available==null) $available = '1';
		$data["available"] = $available;

		// Get Value Of Price Range
		$words = explode("-",$priceRange);
		$words = implode("",array_splice($words,0,2));
		$words = explode("[",$words);
		$words = implode("",array_splice($words,0,3));
		$words = explode("]",$words);
		$words = implode("",array_splice($words,0,2));
		$words = explode(" ",$words);
		$pricelow = $words[2];
		$pricehigh = $words[4];

		// if (!$kasur) $kasur = 0;
		// if (!$lemari) $lemari = 0;
		// if (!$ac) $ac = 0;
		// if (!$kamarmandi) $kamarmandi = 0;
		// if (!$dapur_bersama) $dapur_bersama = 0;
		// if (!$internet) $internet = 0;
		// if (!$keyword) $keyword = "";

		// $data['searchResults'] = $this->properti_model->search($available, $keyword, $pricelow, $pricehigh, $tipe_properti, $occupant_type, $ac, $internet, $lemari, $kasur, $kamarmandi, $dapur_bersama)->getResultArray();
		$data['searchResults'] = $this->properti_model->search($keyword)->getResultArray();

		$number = 0;
		foreach ($data['searchResults'] as $rowResult) {

			if($rowResult['available']!=$available)
				unset($data['searchResults'][$number]);
			
			if($tipe_properti) {
				if($rowResult['tipe_properti']!=$tipe_properti)
					unset($data['searchResults'][$number]);
			}
			if($occupant_type) {
				if($rowResult['occupant_type']!=$occupant_type)
					unset($data['searchResults'][$number]);
			}

			if($dapur_bersama) {
				if($rowResult['dapur_bersama']!=$dapur_bersama)
					unset($data['searchResults'][$number]);
			}
			if($ac) {
				if($rowResult['ac']!=$ac)
					unset($data['searchResults'][$number]);
			}
			if($internet) {
				if($rowResult['internet']!=$internet)
					unset($data['searchResults'][$number]);
			}
			if($lemari) {
				if($rowResult['lemari']!=$lemari)
					unset($data['searchResults'][$number]);
			}
			if($kasur) {
				if($rowResult['kasur']!=$kasur)
					unset($data['searchResults'][$number]);
			}
			if($kamarmandi) {
				if($rowResult['occupant_type']!=$occupant_type)
					unset($data['searchResults'][$number]);
			}

			if($rowResult['price']<$pricelow){
				unset($data['searchResults'][$number]);	
			}
			if($rowResult['price']>$pricehigh){
				unset($data['searchResults'][$number]);	
			}

			$number++;
		}

		// foreach ($data['searchResults'] as $rowResult) {
		// 	echo $rowResult['id'];
		// }
		//  return null;

		session()->setFlashdata('forminputs', $this->request->getPost());
		$data['propertiImg'] = $this->img_model->readImgList();
		$data['contact'] = $this->contact_model->readContact();

		return view('search', $data);
	}

	public function vila()
	{
		$data['title'] = "Vila | CariKos di Pertamina";
		$data['headerTitle'] = "Vila ";
		$data['topnav'] = "Vila";

		$data['properti'] = $this->properti_model->readProperti();
		$data['propertiImg'] = $this->img_model->readImgList();
		$data['contact'] = $this->contact_model->readContact();

		return view('vila', $data);
	}

	public function kos()
	{
		$data['title'] = "Kos | CariKos di Pertamina";
		$data['headerTitle'] = "Kos ";
		$data['topnav'] = "Kos";


		$data['properti'] = $this->properti_model->readProperti();
		$data['propertiImg'] = $this->img_model->readImgList();
		$data['contact'] = $this->contact_model->readContact();

		// $pager = \Config\Services::pager();
		// $data = [
  //           'properti' => $this->properti_model->paginate(1),
  //           'pager' => $this->properti_model->pager
  //       ];

		return view('kos', $data);
	}

	public function kontrakan()
	{
		$data['title'] = "Kontrakan | CariKos di Pertamina";
		$data['headerTitle'] = "Kontrakan ";
		$data['topnav'] = "Kontrakan";

		$data['properti'] = $this->properti_model->readProperti();
		$data['propertiImg'] = $this->img_model->readImgList();
		$data['contact'] = $this->contact_model->readContact();

		return view('kontrakan', $data);
	}

	public function detail($tipeProperti, $id)
	{

		$data['properti'] = $this->properti_model->readOrderedProperti();
		$data['propertiImg'] = $this->img_model->readImgList();

		$data['idProperti'] = $id;
		session()->setFlashdata('inputs', $this->properti_model->readProperti($id)->getRowArray());
		$data['propertiImg'] = $this->img_model->readImgList();
		$data['contact'] = $this->contact_model->readContact();

		$data['title'] = "Detail | CariKos di Pertamina";
		if ($tipeProperti == 'Villa') {
			$data['topnav'] = 'Vila';

		} else {
			$data['topnav'] = $tipeProperti;	
		}

		return view('detail', $data);
	}


	//--------------------------------------------------------------------

}
