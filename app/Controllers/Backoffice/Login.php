<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use App\Models\Admin_model;

class Login extends Controller
{

	public function __construct()
	{
		$this->model = new Admin_model();
		$this->form_validation = \Config\Services::validation();
		$this->session = session();
	}

	public function index()
	{
		if ($this->session->get('logged_in')) return redirect()->to(base_url('/backoffice'));
		echo view('backoffice/login');
	}

	public function auth()
	{
		if ($this->session->get('logged_in')) return redirect()->to(base_url('/backoffice'));
		$username = $this->request->getPost('username');
		$password = $this->request->getPost('password');

		$data = $this->model->getWhere(['username' => $username])->getRowArray();
		$pass = $data['password'];
		$verify_pass = password_verify($password, $pass);
		if (!$verify_pass) {
			session()->setFlashdata('errors', 'Username/Password salah');
			return redirect()->to('/backoffice/login');
		}

		$sesdata = [
			'username'	=> $data['username'],
			'logged_in'	=> TRUE,
		];
		$this->session->set($sesdata);
		return redirect()->to('/backoffice');
	}

	public function logout()
	{
		$this->session->destroy();
		return redirect()->to('/backoffice/login');
	}
}
