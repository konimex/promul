<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use App\Models\Properti_model;
use App\Models\Visitor_model;


class Dashboard extends Controller
{

	public function __construct()
	{
		$this->properti = new Properti_model();
		$this->visitor = new Visitor_model();
		$this->session = session();
	}

	public function index()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));
		$data['kos'] = $this->properti->getWhere(['tipe_properti' => 'Kos'])->getResultArray();
		$data['kontrakan'] = $this->properti->getWhere(['tipe_properti' => 'Kontrakan'])->getResultArray();
		$data['villa'] = $this->properti->getWhere(['tipe_properti' => 'Villa'])->getResultArray();

		$data['jumlahVisitor'] = $this->visitor->countAllResults();

		$data['jumlahKos'] = $this->properti->where(['tipe_properti' => 'Kos'])->countAllResults();
		$data['jumlahKontrakan'] = $this->properti->where(['tipe_properti' => 'Kontrakan'])->countAllResults();
		$data['jumlahVilla'] = $this->properti->where(['tipe_properti' => 'Villa'])->countAllResults();
		echo view('backoffice/dashboard',$data);
	}
}
