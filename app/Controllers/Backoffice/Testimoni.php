<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Testimoni_model;

class Testimoni extends Controller
{

	public function __construct() {
		$this->model = new Testimoni_model();
		$this->form_validation = \Config\Services::validation();
		helper('form');
		helper('filesystem');
	}

	public function index()
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data['testimoni'] = $this->model->readTestimoni();
		echo view('backoffice/testimoni_list', $data);
	}

	public function add_new()
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data['testimoni'] = $this->model->readTestimoni();
		$data['operation'] = "new";
		echo view('backoffice/testimoni_edit', $data);
	}

	public function create()
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$foto = $this->request->getFile('foto');
		$newName = $foto->getRandomName();

		$data = [
			'foto'			=> $newName,
			'name'			=> $this->request->getPost('name'),
			'testimony'		=> $this->request->getPost('testimoni'),
			'modified_date'	=> $currentTime,
		];

		if ($this->form_validation->run($data, 'testimoni') == FALSE) {
			session()->setFlashdata('inputs', $this->request->getPost());
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to('/backoffice/testimoni/add_new');
		}

		$foto->move(ROOTPATH . 'public/uploads/testimoni', $newName);

		$this->model->createTestimoni($data);
		return redirect()->to('/backoffice/testimoni');
	}

	public function edit($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data = $this->model->readTestimoni($id)->getRowArray();
		if (empty(session()->getFlashdata('inputs'))) session()->setFlashdata('inputs', $this->model->readTestimoni($id)->getRowArray());
		$data['operation'] = 'edit';
		echo view('backoffice/testimoni_edit', $data);
	}

	public function update($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$data = [
			'name'			=> $this->request->getPost('name'),
			'testimony'		=> $this->request->getPost('testimoni'),
			'modified_date'	=> $currentTime,
		];

		if ($this->form_validation->run($data, 'testimoni') == FALSE) {
			session()->setFlashdata('input', $this->request->getPost());
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to('/backoffice/testimoni/edit/' . $id);
		}

		if ($this->request->getFile('foto')->isValid()) {
			$foto = $this->request->getFile('foto');
			$newName = $foto->getRandomName();
			$data['foto'] = $newName;
			unlink(ROOTPATH . 'public/uploads/testimoni/'.$this->model->readTestimoni($id)->getRow()->foto);
			$foto->move(ROOTPATH . 'public/uploads/testimoni', $newName);
		}

		$this->model->updateTestimoni($data, $id);
		return redirect()->to('/backoffice/testimoni');
	}

	public function delete($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$foto = $this->model->readTestimoni($id)->getRow()->foto;
		unlink(ROOTPATH . 'public/uploads/testimoni/' . $foto);
		$this->model->deleteTestimoni($id);
		return redirect()->to('/backoffice/testimoni');
	}
}
