<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Contact_model;

class Contact extends Controller
{

	public function __construct()
	{
		$this->model = new Contact_model();
		$this->form_validation = \Config\Services::validation();
		$this->session = session();
	}

	public function index()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('backoffice/login'));
		if (empty(session()->getFlashdata('inputx'))) session()->setFlashdata('inputx', $this->model->readContact(1)->getRowArray());
		echo view('backoffice/contact');
	}

	public function update()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('backoffice/login'));
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$data = [
			'whatsapp'			=> $this->request->getPost('whatsapp'),
			'email'				=> $this->request->getPost('email'),
			'instagram'			=> $this->request->getPost('instagram'),
			'modified_date'		=> $currentTime,
		];

		if ($this->form_validation->run($data, 'contact') == FALSE) {
			session()->setFlashdata('inputx', $this->request->getPost());
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to(base_url('/backoffice/contact'));
		}

		$this->model->updateContact($data, 1);
		session()->setFlashdata('success', 'Kontak berhasil di-update.');
		return redirect()->to('/backoffice/contact');
	}
}
