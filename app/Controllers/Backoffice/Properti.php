<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Properti_model;
use App\Models\PropertiImg_model;

class Properti extends Controller
{
	public function __construct()
	{
		helper('form');
		helper('filesystem');

		$this->properti_model = new Properti_model();
		$this->img_model = new PropertiImg_model();
		$this->form_validation = \Config\Services::validation();
		$this->session = session();
	}

	public function index()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));;
		$data['properti'] = $this->properti_model->readProperti();
		echo view('backoffice/viewproperti',$data);
	}

	public function top_properti()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));;
		$data['properti'] = $this->properti_model->getWhere(['top_property' => '1'])->getResultArray();
		echo view('backoffice/top_properti',$data);
	}

	public function add_top($id)
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');
		$data = [
			'top_property' => '1',
			'modified_date' => $currentTime,
		];

		$this->properti_model->updateProperti($data, $id);
		$data['name'] = $this->properti_model->readProperti($id)->getRow()->name;
		session()->setFlashdata('success', $data['name'] . ' berhasil ditambahkan ke Top Property.');
		return redirect()->to('/backoffice/properti');
	}

	public function del_top($id)
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');
		$data = [
			'top_property' => '0',
			'modified_date' => $currentTime,
		];

		$this->properti_model->updateProperti($data, $id);
		$data['name'] = $this->properti_model->readProperti($id)->getRow()->name;
		session()->setFlashdata('success', $data['name'] . ' berhasil dihapus dari Top Property.');
		return redirect()->to('/backoffice/properti/top_properti');
	}

	public function view($id)
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));;
		$data['id'] = $id;
		$data['operation'] = "view";
		session()->setFlashdata('inputs', $this->properti_model->readProperti($id)->getRowArray());
		echo view('backoffice/isiproperti', $data);
	}

	public function add_new()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));;
		$data['operation'] = "new";
		echo view('backoffice/isiproperti', $data);
	}

	public function create()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$data = [
			'name'				=> $this->request->getPost('name'),
			'tipe_properti'		=> $this->request->getPost('tipe_properti'),
			'occupant_type'		=> $this->request->getPost('occupant_type'),
			'kasur'				=> $this->request->getPost('kasur'),
			'lemari'			=> $this->request->getPost('lemari'),
			'ac'				=> $this->request->getPost('ac'),
			'kamarmandi'		=> $this->request->getPost('kamarmandi'),
			'internet'			=> $this->request->getPost('internet'),
			'dapur_bersama'		=> $this->request->getPost('dapur_bersama'),
			'maks_penghuni'		=> $this->request->getPost('maks_penghuni'),
			'size'				=> $this->request->getPost('size'),
			'price'				=> $this->request->getPost('price'),
			'available'			=> $this->request->getPost('available'),
			'address'			=> $this->request->getPost('address'),
			'other_info'		=> $this->request->getPost('other_info'),
			'top_property'		=> '0',
			'creation_date'		=> $currentTime,
			'modified_date'		=> $currentTime,
		];

		if ($this->form_validation->run($data, 'properti') == FALSE) {
			session()->setFlashdata('inputs', $this->request->getPost());
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to(base_url('backoffice/properti/add_new'));
		}

		if (!$data['kasur']) $data['kasur'] = 0;
		if (!$data['lemari']) $data['lemari'] = 0;
		if (!$data['ac']) $data['ac'] = 0;
		if (!$data['kamarmandi']) $data['kamarmandi'] = 0;
		if (!$data['dapur_bersama']) $data['dapur_bersama'] = 0;
		if (!$data['internet']) $data['internet'] = 0;

		$id_properti = $this->properti_model->createProperti($data);

		$imgs = $this->request->getFiles();

		$rules = [
			'gambar' => 'mime_in[gambar,image/jpg,image/jpeg,image/gif,image/png]|uploaded[gambar]'
		];

		$messages = [
			'gambar' => [
				'uploaded' => 'Gambar harus ada terlebih dahulu',
				'mime_in' => 'Gambar harus berbentuk JPG, GIF, atau PNG',
			],
		];

		$validate_imgs = $this->validate($rules, $messages);

		if (!$validate_imgs) {
			$this->properti_model->deleteProperti($id_properti);
			session()->setFlashdata('inputs', $this->request->getPost());
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to(base_url('backoffice/properti/add_new'));
		}

		foreach($imgs['gambar'] as $img) {
			$newName = $img->getRandomName();
			$img->move(ROOTPATH . 'public/uploads/properti/' . $id_properti . '/', $newName);

			$imgdata = [
				'id_properti'	=> $id_properti,
				'img_name'		=> $newName,
			];

			$this->img_model->createImg($imgdata);
		}

		session()->setFlashdata('success', '<strong>' . $data['name'] . '</strong> berhasil dibuat.');
		return redirect()->to(base_url('/backoffice/properti'));
	}

	public function edit($id)
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));
		$data['operation'] = "edit";
		$data['id'] = $id;
		if (empty(session()->getFlashdata('inputs'))) session()->setFlashdata('inputs', $this->properti_model->readProperti($id)->getRowArray());
		echo view('backoffice/isiproperti', $data);
	}

	public function update($id)
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));

		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$data = [
			'name'				=> $this->request->getPost('name'),
			'tipe_properti'		=> $this->request->getPost('tipe_properti'),
			'occupant_type'		=> $this->request->getPost('occupant_type'),
			'kasur'				=> $this->request->getPost('kasur'),
			'lemari'			=> $this->request->getPost('lemari'),
			'ac'				=> $this->request->getPost('ac'),
			'kamarmandi'		=> $this->request->getPost('kamarmandi'),
			'internet'			=> $this->request->getPost('internet'),
			'dapur_bersama'		=> $this->request->getPost('dapur_bersama'),
			'maks_penghuni'		=> $this->request->getPost('maks_penghuni'),
			'size'				=> $this->request->getPost('size'),
			'price'				=> $this->request->getPost('price'),
			'available'			=> $this->request->getPost('available'),
			'address'			=> $this->request->getPost('address'),
			'other_info'		=> $this->request->getPost('other_info'),
			'modified_date'		=> $currentTime,
		];

		if ($this->form_validation->run($data, 'properti') == FALSE) {
			session()->setFlashdata('inputs', $this->request->getPost());
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to(base_url('/backoffice/properti/edit/' . $id));
		}

		if (!$data['kasur']) $data['kasur'] = 0;
		if (!$data['lemari']) $data['lemari'] = 0;
		if (!$data['ac']) $data['ac'] = 0;
		if (!$data['kamarmandi']) $data['kamarmandi'] = 0;
		if (!$data['dapur_bersama']) $data['dapur_bersama'] = 0;
		if (!$data['internet']) $data['internet'] = 0;

		$this->properti_model->updateProperti($data, $id);
		session()->setFlashdata('success', $data['name'] . 'berhasil di-update.');
		return redirect()->to('/backoffice/properti');
	}

	public function delete($id)
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));
		delete_files(ROOTPATH . 'public/uploads/properti/' . $id . '/');
		$this->img_model->deleteImgList($id);
		$this->properti_model->deleteProperti($id);
		session()->setFlashdata('success', 'Properti berhasil dihapus.');
		return redirect()->to('/backoffice/properti');
	}
}
