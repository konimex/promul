<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Top_model;

class Slider extends Controller
{

	public function __construct()
	{
		helper('form');
		helper('filesystem');
		$this->model = new Top_model();
		$this->form_validation = \Config\Services::validation();
		$this->session = session();
	}

	public function index()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('backoffice/login'));
		$data['slider'] = $this->model->readTop();
		echo view('backoffice/slider', $data);
	}

	/*
	public function new()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));
		if (!$this->validate([])) {
			$data['validation'] = $this->validator;
		}

		$data['operation'] = "new";
		echo view('backoffice/sliderupload', $data);
	}
	*/

	public function edit($id)
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('/backoffice/login'));
		$data['id'] = $id;
		$data['operation'] = "edit";
		echo view('backoffice/sliderupload', $data);
	}

	/*
	public function create()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('backoffice/login'));
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$validated = $this->validate([
			'gambar' => 'uploaded[gambar]|mime_in[gambar,image/jpg,image/jpeg,image/gif,image/png]'
		]);

		if (!$validated) {
			return redirect()->to('/backoffice/slider/new');
		}

		$gambar = $this->request->getFile('gambar');
		$newName = $gambar->getRandomName();
		$gambar->move(ROOTPATH . 'public/uploads/slider', $newName);
		$data = [
			'img_name'		=> $newName,
			'modified_date'	=> $currentTime,
		];
		$this->model->createTop($data);
		return redirect()->to('/backoffice/slider');
	}
	*/

	public function update($id)
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('backoffice/login'));
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$rules = [
			'gambar' => 'mime_in[gambar,image/jpg,image/jpeg,image/gif,image/png]'
		];

		$messages = [
			'gambar' => [
				'mime_in' => 'Gambar harus berbentuk JPG, GIF, atau PNG',
			],
		];

		$validated = $this->validate($rules, $messages);

		if (!$validated) {
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to('/backoffice/slider/edit/'.$id);
		}

		$gambar = $this->request->getFile('gambar');
		$newName = $gambar->getRandomName();
		unlink(ROOTPATH . 'public/uploads/slider/'.$this->model->readTop($id)->getRow()->img_name);
		$gambar->move(ROOTPATH . 'public/uploads/slider', $newName);
		$data = [
			'img_name'		=> $newName,
			'modified_date'	=> $currentTime,
		];
		$this->model->updateTop($data, $id);
		return redirect()->to('/backoffice/slider');
	}
}
