<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Admin_model;

class Admin extends Controller
{

	public function __construct()
	{
		$this->model = new Admin_model();
		$this->form_validation = \Config\Services::validation();
	}

	public function index()
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');

		session()->setFlashdata('inputs', $this->model->readAdmin(1)->getRow()->username);
		echo view('backoffice/profile');
	}
	/*
	public function create()
	{
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');


		$username = $this->request->getPost('username');
		$password = $this->request->getPost('password');
		$confirm  = $this->request->getPost('confirm');

		$validatethis = [
			'username'	=> $username,
			'password'	=> $password,
			'confirm'	=> $confirm,
		];

		if ($this->form_validation->run($validatethis, 'admin') == FALSE) {
			session()->setFlashdata('input', $this->request->getPost('username'));
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			//return redirect()->to(base_url('kos'));
		}

		$data = [
			'username'		=> $username,
			'password'		=> password_hash($password, PASSWORD_BCRYPT),
			'modified_date'	=> $currentTime,
		];

		$this->model->createAdmin($data);
		// return redirect()->to(base_url('/kos'));
	}
*/
	public function update()
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');


		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$username = $this->request->getPost('username');
		$oldpass  = $this->request->getPost('oldpass');
		$password = $this->request->getPost('password');
		$confirm  = $this->request->getPost('confirm');

		$validatethis = [
			'username'	=> $username,
			'oldpass'	=> $oldpass,
			'password'	=> $password,
			'confirm'	=> $confirm,
		];

		$olddata = $this->model->readAdmin(1)->getRowArray();
		if (!password_verify($oldpass, $olddata['password'])) {
			session()->setFlashdata('errors', ['Password lama Anda salah']);
			return redirect()->to(base_url('backoffice/admin'));
		}

		if ($this->form_validation->run($validatethis, 'admin') == FALSE) {
			session()->setFlashdata('inputs', $this->request->getPost('username'));
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to(base_url('backoffice/admin'));
		}

		$data = [
			'username'		=> $username,
			'password'		=> password_hash($password, PASSWORD_BCRYPT),
			'modified_date'	=> $currentTime,
		];

		session()->set('username', $username);
		$this->model->updateAdmin($data, 1);
		$data['inputs'] = $this->model->readAdmin(1);
		session()->setFlashdata('success', 'Username/password Anda berhasil diganti');
		return redirect()->to(base_url('backoffice'));
	}
}
