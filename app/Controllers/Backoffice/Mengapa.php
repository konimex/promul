<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use App\Models\Mengapa_model;

class Mengapa extends Controller
{

	public function __construct()
	{
		$this->model = new Mengapa_model();
		$this->form_validation = \Config\Services::validation();
		$this->session = session();
		helper('Form');
		helper('Filesystem');
	}

	public function index()
	{
		if (!$this->session->get('logged_in')) return redirect()->to(base_url('backoffice/login'));

		$data['mengapa'] = $this->model->readMengapa();
		echo view('backoffice/mengapa_list',$data);
	}

	public function edit($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data = $this->model->readMengapa($id)->getRowArray();
		if (empty(session()->getFlashdata('inputs'))) session()->setFlashdata('inputs', $this->model->readMengapa($id)->getRowArray());
		$data['operation'] = 'edit';
		echo view('backoffice/mengapa_edit', $data);
	}

	public function update($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$currentTime = new Time('now', 'Asia/Jakarta', 'en_US');

		$data = [
			'title'			=> $this->request->getPost('title'),
			'description'	=> $this->request->getPost('description'),
			'modified_date'	=> $currentTime,
		];

		if ($this->request->getFile('icon')->isValid()) {
			$icon = $this->request->getFile('icon');

			$rules = [
				'icon' => 'mime_in[icon,image/jpg,image/jpeg,image/gif,image/png]'
			];

			$messages = [
				'icon' => [
					'mime_in' => 'Icon harus berbentuk JPG, GIF, atau PNG',
				],
			];

			$validated = $this->validate($rules, $messages);

			if (!$validated) {
				session()->setFlashdata('errors', $this->form_validation->getErrors());
				return redirect()->to('/backoffice/mengapa/edit/'.$id);
			}
			$newName = $icon->getRandomName();
			$data['icon'] = $newName;
			unlink(ROOTPATH . 'public/uploads/mengapa/'.$this->model->readMengapa($id)->getRow()->icon);
			$icon->move(ROOTPATH . 'public/uploads/mengapa', $newName);
		}

		$this->model->updateMengapa($data, $id);
		return redirect()->to('/backoffice/mengapa');
	}
}
