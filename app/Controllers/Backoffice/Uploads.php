<?php namespace App\Controllers\Backoffice;

use CodeIgniter\Controller;
use App\Models\PropertiImg_model;
use App\Models\Properti_model;

class Uploads extends Controller
{

	public function __construct()
	{
		helper('form');
		$this->model = new PropertiImg_model();
		$this->properti_model = new Properti_model();
		$this->form_validation = \Config\Services::validation();
	}

	public function index()
	{
		return redirect()->to('/backoffice');
	}

	public function view($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data['id'] = $id;
		$data['images'] = $this->model->readImgList($id)->getResultArray();
		$data['name'] = $this->properti_model->readProperti($id)->getRow()->name;
		$data['operation'] = 'view';
		echo view('backoffice/images', $data);
	}

	public function edit($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data['id'] = $id;
		$data['images'] = $this->model->readImgList($id)->getResultArray();
		$data['name'] = $this->properti_model->readProperti($id)->getRow()->name;
		$data['operation'] = 'edit';
		echo view('backoffice/images', $data);
	}

	public function new($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data['id_properti'] = $this->properti_model->readProperti($id)->getRow()->id;
		$data['name'] = $this->properti_model->readProperti($data['id_properti'])->getRow()->name;
		$data['operation'] = 'new';
		echo view('backoffice/imagesupload.php', $data);
	}

	public function imgview($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data['id_properti'] = $this->model->readImg($id)->getRow()->id_properti;
		$data['img_name'] = $this->model->readImg($id)->getRow()->img_name;
		$data['name'] = $this->properti_model->readProperti($data['id_properti'])->getRow()->name;
		$data['operation'] = 'view';
		echo view('backoffice/imagesupload.php', $data);
	}

	// Yang ini View buat Edit
	public function imgpreview($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data['id_properti'] = $this->model->readImg($id)->getRow()->id_properti;
		$data['img_name'] = $this->model->readImg($id)->getRow()->img_name;
		$data['name'] = $this->properti_model->readProperti($data['id_properti'])->getRow()->name;
		$data['operation'] = 'preview';
		echo view('backoffice/imagesupload.php', $data);
	}

	public function imgnew($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$data['id'] = $this->model->readImg($id)->getRow()->id;
		$data['id_properti'] = $this->model->readImg($id)->getRow()->id_properti;
		$data['name'] = $this->properti_model->readProperti($data['id_properti'])->getRow()->name;
		$data['operation'] = 'edit';
		echo view('backoffice/imagesupload.php', $data);
	}

	public function create($id_properti)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$imgs = $this->request->getFiles();

		$rules = [
			'gambar' => 'mime_in[gambar,image/jpg,image/jpeg,image/gif,image/png]|uploaded[gambar]'
		];

		$messages = [
			'gambar' => [
				'uploaded' => 'Gambar harus ada terlebih dahulu',
				'mime_in' => 'Gambar harus berbentuk JPG, GIF, atau PNG',
			],
		];

		$validate = $this->validate($rules, $messages);

		if (!$validate) {
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to(base_url('backoffice/uploads/new/' . $id_properti));
		}

		foreach($imgs['gambar'] as $img) {
			$newName = $img->getRandomName();
			$img->move(ROOTPATH . 'public/uploads/properti/' . $id_properti . '/', $newName);

			$data = [
				'id_properti'	=> $id_properti,
				'img_name'		=> $newName,
			];

			$this->model->createImg($data);
		}

		session()->setFlashdata('success', 'Gambar berhasil di-upload.');
		return redirect()->to('/backoffice/uploads/edit/'. $id_properti);
	}

	public function update($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$img = $this->request->getFile('gambar');

		$rules = [
			'gambar' => 'mime_in[gambar,image/jpg,image/jpeg,image/gif,image/png]|uploaded[gambar]'
		];

		$messages = [
			'gambar' => [
				'uploaded' => 'Gambar harus ada terlebih dahulu',
				'mime_in' => 'Gambar harus berbentuk JPG, GIF, atau PNG',
			],
		];

		$validate = $this->validate($rules, $messages);

		if (!$validate) {
			session()->setFlashdata('errors', $this->form_validation->getErrors());
			return redirect()->to(base_url('backoffice/uploads/imgnew/' . $id));
		}

		$id_properti = $this->model->readImg($id)->getRow()->id_properti;
		$img_name = $this->model->readImg($id)->getRow()->img_name;

		unlink(ROOTPATH . 'public/uploads/properti/' . $id_properti . '/'. $img_name);
		$img->move(ROOTPATH . 'public/uploads/properti/' . $id_properti . '/', $img_name);

		session()->setFlashdata('success', 'Gambar berhasil di-update.');
		return redirect()->to('/backoffice/uploads/edit/'. $id_properti);
	}

	public function delete($id)
	{
		if (!session()->get('logged_in')) return redirect()->to('/backoffice');
		$id_properti	= $this->model->readImg($id)->getRow()->id_properti;
		$img_name		= $this->model->readImg($id)->getRow()->img_name;
		$this->model->deleteImg($id);
		unlink(ROOTPATH . 'public/uploads/properti/' . $id_properti . '/' . $img_name);
		return redirect()->to('/backoffice/uploads/edit/'. $id_properti);
	}
}
