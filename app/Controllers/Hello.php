<?php namespace App\Controllers;
use CodeIgniter\Controller;

class Hello extends Controller
{
    public function index()
    {
        $data['title'] = "Hello, world";
        echo view('welcome_message', $data);
    }
}
