<?php namespace App\Models;
use CodeIgniter\Model;

class Admin_model extends Model
{
	protected $table = 'admin';

	public function readAdmin($id = false)
	{
		if ($id === false) {
			return $this->findAll();
		} else {
			return $this->getWhere(['id' => $id]);
		}
	}

	public function createAdmin($data)
	{
		$query = $this->db->table($this->table)->insert($data);
		return $query;
	}

	public function updateAdmin($data, $id)
	{
		$query = $this->db->table($this->table)->update($data, ['id' => $id]);
		return $query;
	}

	public function deleteAdmin($id)
	{
		$query = $this->db->table($this->table)->delete(['id' => $id]);
		return $query;
	}
}
