<?php namespace App\Models;
use CodeIgniter\Model;

class Contact_model extends Model
{
	protected $table = 'contact';

	public function readContact($id = false)
	{
		if ($id === false) {
			return $this->findAll();
		} else {
			return $this->getWhere(['id' => $id]);
		}
	}

	public function createContact($data)
	{
		$query = $this->db->table($this->table)->insert($data);
		return $query;
	}

	public function updateContact($data, $id)
	{
		$query = $this->db->table($this->table)->update($data, array('id' => $id));
		return $query;
	}

	public function deleteContact($id)
	{
		$query = $this->db->table($this->table)->delete(array('id' => $id ));
		return $query;
	}
}
