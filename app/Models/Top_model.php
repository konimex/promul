<?php namespace App\Models;
use CodeIgniter\Model;

class Top_model extends Model
{
	protected $table = 'top_home';

	public function readTop($id = false)
	{
		if ($id === false) {
			return $this->findAll();
		} else {
			return $this->getWhere(['id' => $id]);
		}
	}

	public function createTop($data)
	{
		$query = $this->db->table($this->table)->insert($data);
		return $query;
	}

	public function updateTop($data, $id)
	{
		$query = $this->db->table($this->table)->update($data, array('id' => $id));
		return $query;
	}

	public function deleteTop($id)
	{
		$query = $this->db->table($this->table)->delete(array('id' => $id ));
		return $query;
	}
}
