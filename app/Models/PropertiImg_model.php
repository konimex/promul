<?php namespace App\Models;
use CodeIgniter\Model;

class PropertiImg_model extends Model
{
	protected $table = 'properti_img';

	public function readImgList($id = false)
	{
		if ($id === false) {
			return $this->findAll();
		} else {
			return $this->getWhere(['id_properti' => $id]);
		}
	}

	public function readImg($id)
	{
		return $this->getWhere(['id' => $id]);
	}

	public function createImg($data)
	{
		$query = $this->db->table($this->table)->insert($data);
		return $query;
	}

	public function updateImg($data, $id)
	{
		$query = $this->db->table($this->table)->update($data, ['id' => $id]);
		return $query;
	}

	public function deleteImgList($id)
	{
		$query = $this->db->table($this->table)->delete(['id_properti' => $id]);
		return $query;
	}

	public function deleteImg($id)
	{
		$query = $this->db->table($this->table)->delete(['id' => $id]);
		return $query;
	}
}
