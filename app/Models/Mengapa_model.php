<?php namespace App\Models;
use CodeIgniter\Model;

class Mengapa_model extends Model
{
	protected $table = 'mengapa_home';

	public function readMengapa($id = false)
	{
		if ($id === false) {
			return $this->findAll();
		} else {
			return $this->getWhere(['id' => $id]);
		}
	}

	public function createMengapa($data)
	{
		$query = $this->db->table($this->table)->insert($data);
		return $query;
	}

	public function updateMengapa($data, $id)
	{
		$query = $this->db->table($this->table)->update($data, array('id' => $id));
		return $query;
	}

	public function deleteMengapa($id)
	{
		$query = $this->db->table($this->table)->delete(array('id' => $id ));
		return $query;
	}
}
