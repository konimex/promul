<?php namespace App\Models;
use CodeIgniter\Model;

class Testimoni_model extends Model
{
	protected $table = 'testimoni_home';

	public function readTestimoni($id = false)
	{
		if ($id === false) {
			return $this->findAll();
		} else {
			return $this->getWhere(['id' => $id]);
		}
	}

	public function createTestimoni($data)
	{
		$query = $this->db->table($this->table)->insert($data);
		return $query;
	}

	public function updateTestimoni($data, $id)
	{
		$query = $this->db->table($this->table)->update($data, array('id' => $id));
		return $query;
	}

	public function deleteTestimoni($id)
	{
		$query = $this->db->table($this->table)->delete(array('id' => $id));
		return $query;
	}
}
