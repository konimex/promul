<?php namespace App\Models;
use CodeIgniter\Model;

class Properti_model extends Model
{
	protected $table = 'properti';

	public function readProperti($id = false)
	{
		if ($id === false) {
			return $this->findAll();
		} else {
			return $this->getWhere(['id' => $id]);
		}
	}

	public function createProperti($data)
	{
		$query = $this->db->table($this->table)->insert($data);
		// Intelephense shows as error, ignore this.
		return $this->db->insertID();
	}

	public function updateProperti($data, $id)
	{
		$query = $this->db->table($this->table)->update($data, array('id' => $id));
		return $query;
	}

	public function deleteProperti($id)
	{
		$query = $this->db->table($this->table)->delete(array('id' => $id ));
		return $query;
	}

	public function readOrderedProperti()
	{
		return $this->orderBy('id', 'DESC')->findAll();
	}

	public function search($keyword) {
		
		$query = $this->db->table($this->table)
		->like('address', $keyword)
		->orlike('name', $keyword)
		->orlike('other_info', $keyword);

		return $query->get();
	}

	// public function search($available, $keyword, $pricelow, $pricehigh, $tipe_properti = false, $occupant_type = false, $ac = false, $internet = false, $lemari = false, $kasur = false, $kamarmandi = false, $dapur_bersama = false) {
		
	// 	$query = $this->db->table($this->table)
	// 	->orlike('address', $keyword)
	// 	->orlike('name', $keyword)
	// 	->orlike('other_info', $keyword);

	// 	if ($tipe_properti) $query = $query->where('tipe_properti', $tipe_properti);
	// 	if ($occupant_type) $query = $query->where('occupant_type', $occupant_type);

	// 	if ($ac) $query = $query->where('ac', $ac);
	// 	if ($lemari) $query = $query->where('lemari', $lemari);
	// 	if ($internet) $query = $query->where('internet', $internet);
	// 	if ($kasur) $query = $query->where('kasur', $kasur);
	// 	if ($kamarmandi) $query = $query->where('kamarmandi', $kamarmandi);
	// 	if ($dapur_bersama) $query = $query->where('dapur_bersama', $dapur_bersama);

	// 	return $query->get();
	// }
}
