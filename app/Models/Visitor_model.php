<?php namespace App\Models;
use CodeIgniter\Model;

class Visitor_model extends Model
{
	protected $table = 'visitor';

	public function readVisitor($id = false)
	{
		if ($id === false) {
			return $this->findAll();
		} else {
			return $this->getWhere(['id' => $id]);
		}
	}

	public function createVisitor($data)
	{
		$query = $this->db->table($this->table)->insert($data);
		return $query;
	}

	public function updateVisitor($data, $id)
	{
		$query = $this->db->table($this->table)->update($data, array('id' => $id));
		return $query;
	}

	public function deleteVisitor($id)
	{
		$query = $this->db->table($this->table)->delete(array('id' => $id ));
		return $query;
	}
}
