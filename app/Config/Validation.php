<?php namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------

	public $admin = [
		'username'		=> [
			'rules'		=> 'required',
			'errors'	=> [
				'required'		=> 'Username wajib diisi',
			],
		],

		'password'	=> [
			'rules'		=> 'required|min_length[6]|max_length[72]',
			'errors'	=> [
				'required'		=> 'Password wajib diisi',
				'min_length'	=> 'Password Anda terlalu singkat, minimal 6 karakter',
				'max_length'	=> 'Password Anda terlalu panjang, maksimal 72 karakter',
			],
		],

		'confirm'	=> [
			'rules'		=> 'required|matches[password]',
			'errors'	=> [
				'required'		=> 'Confirm password wajib diisi',
				'matches'		=> 'Password Anda dan Konfirmasi Password tidak sama',
			],
		],
	];

	public $properti = [
		'name' 			=> [
			'rules' 	=> 'required',
			'errors' 	=> [
				'required'				=> 'Nama properti wajib diisi',
			],
		],

		'tipe_properti'	=> [
			'rules'		=> 'required',
			'errors'	=> [
				'required'				=> 'Tipe properti wajib diisi',
			],
		],

		'occupant_type' => [
			'rules' 	=> 'required',
			'errors' 	=> [
				'required'				=> 'Tipe penghuni wajib dipilih',
			],
		],

		'maks_penghuni' => [
			'rules' 	=> 'required|numeric|is_natural',
			'errors' 	=> [
				'required'				=> 'Penghuni maksimum wajib diisi',
				'is_natural'			=> 'Penghuni maksimum tidak boleh berisi angka desimal/angka minus',
			],
		],

		'price'			=> [
			'rules' 	=> 'required|numeric',
			'errors' 	=> [
				'required'				=> '<b>Harga</b> properti wajib diisi',
				'numeric'				=> 'Harga properti hanya boleh diisi dengan angka',
			],
		],

		'available'		=> [
			'rules'		=> 'required',
			'errors'	=> [
				'required'				=> 'Status kesediaan wajib diisi',
			],
		],

		'address'		=> [
			'rules'		=> 'required',
			'errors'	=> [
				'required'				=> 'Alamat wajib diisi',
			],
		],
	];

	public $testimoni = [
		'name' 			=> [
			'rules' 	=> 'required|alpha_numeric_space',
			'errors' 	=> [
				'required'				=> 'Nama pemberi testimoni wajib diisi',
				'alpha_numeric_space'	=> 'Nama pemberi testimoni hanya boleh diisi dengan huruf, angka, dan spasi',
			],
		],

		'testimony'		=> [
			'rules'		=> 'required',
			'errors'	=> [
				'required'				=> 'Testimoni wajib diisi',
			]
		],

		'foto'			=> [
			'rules' => 'mime_in[foto,image/jpg,image/jpeg,image/gif,image/png]',
			'errors' => [
				'mime_in' => 'Foto harus berbentuk JPG, GIF, atau PNG',
			],
		],
	];

	public $contact = [
		'whatsapp'		=> [
			'rules'		=> 'required|numeric',
			'errors'	=> [
				'required'				=> 'WhatsApp wajib diisi',
				'numeric'				=> 'WhatsApp hanya boleh diisi angka',
			],
		],

		'email'			=> [
			'rules'		=> 'required|valid_email',
			'errors'	=> [
				'required'				=> 'E-mail wajib diisi',
				'valid_email'			=> 'E-mail tidak valid',
			],
		],

		'instagram'		=> [
			'rules'		=> 'required',
			'errors'	=> [
				'required'				=> 'Instagram wajib diisi',
			],
		],
	];

	public $gambar = [
		'gambar'		=> [
			'rules'		=> 'mime_in[gambar,image/jpg,image/jpeg,image/gif,image/png]',
			'errors'	=> [
				'mime_in' => 'Gambar harus berbentuk JPG, GIF, atau PNG',
			],
		],
	];
}
